def sroot(sqr, prec):
    guestOne = sqr/2
    holdNum = 0
    while(abs(holdNum) <= prec):
        holdNum = (guestOne * guestOne) - sqr
        guestOne = (guestOne + sqr) / guestOne
    return guestOne
print(sroot(9, 2.0))