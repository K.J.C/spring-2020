def displayProgramTitle(firstName, lastName):
    print(f"Display Weekdays Program by {firstName} {lastName}")


def displayList(ListName, list):
    print(ListName)

    for element in list:
        print(" " + str(element))


def main():
    weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wendesday', 'Thursday', 'Friday', 'Saturday']

    # display Program Title
    displayProgramTitle('Kyler', 'Carling')
    # display the list with header
    displayList("Days of the week", weekDays)
    # print the program completion message: "Display weekdays program completed".
    #
    print("Display weekdays program completed")
    #


if __name__ == '__main__': main()
