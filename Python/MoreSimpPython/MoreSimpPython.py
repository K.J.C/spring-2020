import math


def sohcahtoa(length_a, length_b):
    returnValueOne = math.degrees(math.atan(length_a))
    returnValueTwo = math.degrees(math.atan(length_b))
    return (returnValueOne, returnValueTwo)


def occurrence(list_in, value):
    return list_in.count(value)


def wordList(string):
    startIndex = 0
    endIndex = 0
    wordList = []
    for char in string:
        if char == ' ':
            subString = string[startIndex:endIndex]
            if not subString in wordList:
                wordList.append(subString)
            startIndex = endIndex
        endIndex += 1
    # gets the last word into the list because it has no space after it
    if not string[startIndex:endIndex] in wordList:
        wordList.append(string[startIndex:endIndex])
    return wordList

print("------Sohcahtoa Method Test------")
print(sohcahtoa(2, 10))
print(sohcahtoa(12, 6))
print(sohcahtoa(1, 1))

print("------Occurrence Method Test------")
print(occurrence([1, 4, 7, 6, 4, 7, 4, 3, 3, 2, 9], 4))
print(occurrence([6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6], 6))
print(occurrence([1, 4, 7, 6, 4, 7, 4, 3, 3, 2, 9], -1))

print("------WordList Method Test------")
print(wordList("This is the the test to string to my hart hart hart"))
print(wordList("I'm going to the store, to the store, and I ain't coming back no more"))
print(wordList("This sentence contains no duplicate words to delete."))
