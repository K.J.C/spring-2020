def main():
    daysOfTheWeek= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    firstName="Kyler"
    lastName="Carling"

    print("Display weekdays program by " + firstName + " "+ lastName)
    print("Days of the week:")

    for currentDay in daysOfTheWeek:
        print("   "+currentDay)
    print("Display weekdays program completed")

main()