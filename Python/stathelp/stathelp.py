import statistics

def ascending(toBeOrdered):
    toBeOrdered.sort()
    print(toBeOrdered)

def decending(toBeOrdered):
    toBeOrdered.sort(reverse=True)
    print(toBeOrdered)

def outliers():
    print()

# The number you want to process
numbers = [6,7,8,10,11,12,14,17,18,19,22]


# method call area below
ascending(numbers)
mean = statistics.mean(numbers)
median = statistics.median(numbers)
standardDeviation = statistics.stdev(numbers,mean)
mean = outliers()
print("Mean: "+str(mean))
print("Median: "+str(median))
print("StDev: "+str(standardDeviation))

