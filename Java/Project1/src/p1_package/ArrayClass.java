package p1_package;

/**
 * Description: Class wrapper for a Java array,
 * with additional management operations
 * <p>
 * Note: Maintains a capacity value for maximum number of
 * items that can be stored,
 * and a size value for the number of valid or viable data items in the array
 *
 * @author  Kyler Carling
 * @version 1.1
 * @since   Jan 13 2020
 */
public class ArrayClass
{
    /**
     * Maximum Possible Size
     */
    private int arrayCapacity;
    /**
     * Current number of valid values
     */
    private int arraySize;
    /**
     * Default value of the capacity variable
     */
    private static final int DEFAULT_CAPACITY=10;
    /**
     * Exit code indicated a failure to access the requested resource
     */
    public static final int FAILED_ACCESS=-999999;
    /**
     * The actual that this program wraps around
     */
    private int[] localArray;

    /**
     * Default constructor, initializes array to default capacity
     */

    public ArrayClass()
    {
        arrayCapacity=DEFAULT_CAPACITY;
        arraySize=0;
        localArray= new int[DEFAULT_CAPACITY];
    }

    /**
     * Copy constructor,
     * initializes array to size and capacity of copied array,
     * then copies only the elements up to the given size
     * @param copied  ArrayClass object to be copied
     */

    public ArrayClass(ArrayClass copied)
    {
        this.arraySize=copied.getCurrentSize();
        this.arrayCapacity=copied.getCurrentCapacity();
        localArray = new int[getCurrentCapacity()];
        int arrayIterator=0;
        while(arrayIterator<arraySize)
        {
            this.localArray[arrayIterator]=copied.accessItemAt(arrayIterator);
            arrayIterator++;
        }
    }

    /**
     * Initializing constructor, initializes array to specified capacity
     * @param capacity integer maximum capacity specification for the array
     */

    public ArrayClass(int capacity)
    {
        arrayCapacity=capacity;
        arraySize=0;
        localArray= new int[arrayCapacity];
    }

    /**
     * Initializing constructor, initializes array to specified capacity,
     * size to specified value,
     * then fills all elements with specified size value
     * @param capacity maximum capacity specification for the array
     * @param size sets the number of items to be filled in array,
     *            and sets the size of the ArrayClass object <p>
     * @param fillValue value to be placed in all elements <p>
     *                  of initialized array up to the size
     */

    public ArrayClass(int capacity, int size, int fillValue)
    {
        arrayCapacity=capacity;
        localArray= new int[arrayCapacity];
        arraySize=size;
        int localArrayIterator=0;
        while(localArrayIterator<=arraySize)
        {
            localArray[localArrayIterator]=fillValue;
            localArrayIterator++;
        }
    }

    /**
     * Accesses item in array at specified index
     * if index within array size bounds
     * @param accessIndex index of requested element value
     * @return accessed value if successful, FAILED_ACCESS (-999999) if not
     */

    public int accessItemAt(int accessIndex)
    {
        if(accessIndex<getCurrentSize())
        {
            return this.localArray[accessIndex];
        }
        return FAILED_ACCESS;
    }

    /**
     * Appends item to end of array, if array is not full,
     *  e.g., no more values can be added
     * @param newValue value to be appended to array
     * @return Boolean success if appended, or fai        System.out.println(defaultTester.accessItemAt(10));lure if array was full
     */

    public boolean appendItem(int newValue)
    {
        if(isFull())
        {
            return false;
        }
        // Adds newValue to position at size and increments size
        arraySize++;
        int indexToAppendValue=getCurrentSize()-1;
        localArray[indexToAppendValue]=newValue;
        return true;
    }

    /**
     * Clears array of all valid values by setting array size to zero,
     * values remain in array but are not accessible
     */

    public void clear()
    {
        arraySize=0;
    }

    /**
     * Description: Gets current capacity of array
     *<p>
     * Note: capacity of array indicates number of values the array can hold
     * @return capacity of array
     */

    public int getCurrentCapacity()
    {
        return arrayCapacity;
    }

    /**
     * Description: Gets current size of array
     *<p>
     * Note: size of array indicates number
     * of valid or viable values in the array
     * @return size of array
     */

    public int getCurrentSize()
    {
        return arraySize;
    }

    /**
     * Description: Inserts item to array at specified index
     * if array is not full, e.g., no more values can be added
     *<p>
     * Note: Value is inserted at given index,
     * all data from that index to the end of the array is shifted up by one
     *<p>
     * Note: Value can be inserted after the last valid element
     * but not at any index past that point
     * @param insertIndex index of element into which value is to be inserted
     * @param newValue value to be inserted into array
     * @return Boolean success if inserted, or failure if array was full
     */

    public boolean insertItemAt(int insertIndex, int newValue)
    {
        //Gatekeeper block to throw out bad input
        if(isFull() || insertIndex>getCurrentSize()+1 || insertIndex<0)
        {
            return false;
        }
        arraySize++;
        int positionIndex=getCurrentSize()-1;

        // 1 offset to prevent numbers from being pushed out of array.
        if(positionIndex==getCurrentCapacity()-1)
        {
            positionIndex--;
        }

        while(positionIndex>=insertIndex)
        {
                localArray[positionIndex+1]=localArray[positionIndex];
                positionIndex--;
        }
        localArray[insertIndex]=newValue;
        return true;
    }

    /**
     * Tests for size of array equal to capacity, no more values can be added
     * @return Boolean result of test for full
     */

    public boolean isFull()
    {
        return getCurrentSize()==getCurrentCapacity();
    }

    /**
     * Tests for size of array equal to zero, no valid values stored in array
     * @return Boolean result of test for empty
     */

    public boolean isEmpty()
    {
        return getCurrentSize()==0;
    }

    /**
     * Removes item from array at specified index
     * if index within array size bounds
     * <p>
     * Note: Each data item from the element immediately
     * above the remove index to the end of the array
     * is moved down by one element
     * @param removeIndex index of element value to be removed
     * @return the value contained at the removeIndex, otherwise FAILED_ACCESS
     */

    public int removeItemAt(int removeIndex)
    {
        //Gatekeeper block to throw out bad input
        if(isEmpty() || removeIndex<0 || removeIndex>getCurrentSize()-1)
        {
            return FAILED_ACCESS;
        }

        int indexToDelete=accessItemAt(removeIndex);

        int oldPositionIndex=removeIndex;
        int newPositionIndex=oldPositionIndex+1;
        while(oldPositionIndex<getCurrentSize()-1)
        {
            localArray[oldPositionIndex]=localArray[newPositionIndex];
            oldPositionIndex++;
            newPositionIndex++;
        }
        arraySize--;
        return indexToDelete;
    }



    /**
     * Description: Resets array capacity, copies current size
     * and current size number of elements
     *<p>
     * Exception: Method will not resize capacity
     * below current array capacity,
     * returns false if this is attempted, true otherwise
     * new array copies the size and contents of the old array
     * @param newCapacity new capacity to be set;
     * must be larger than current capacity
     * @return Boolean condition of resize success or failure
     */

    public boolean resize(int newCapacity)
    {
        if(newCapacity<=getCurrentCapacity())
        {
            return false;
        }

        int[] resizedArr = new int[newCapacity];
        int arrayIterator=0;
        while(arrayIterator<getCurrentSize())
        {
            resizedArr[arrayIterator]=accessItemAt(arrayIterator);
            arrayIterator++;
        }
        arrayCapacity=newCapacity;

        this.localArray=resizedArr;
        return true;
    }


}
