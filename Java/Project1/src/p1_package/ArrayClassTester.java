package p1_package;

public class ArrayClassTester
{
    public static void main(String[] args)
    {

        int index;

        ArrayClass defaultTester = new ArrayClass();
        for(index=0;index<defaultTester.getCurrentCapacity();index++)
        {
            defaultTester.appendItem(index);
        }
        print(defaultTester);
        System.out.println(defaultTester.removeItemAt(16));
        System.out.println(defaultTester.removeItemAt(0));
        System.out.println(defaultTester.removeItemAt(3));
        System.out.println(defaultTester.removeItemAt(5));

        print(defaultTester);


        // cap block
        ArrayClass capacityTester = new ArrayClass(5);

        for(index=0;index<capacityTester.getCurrentCapacity();index++)
        {
            capacityTester.insertItemAt(index,index);
        }
        print(capacityTester);
        capacityTester.resize(7);
        for(index=0;index<capacityTester.getCurrentCapacity();index++)
        {
            capacityTester.insertItemAt(index,index);
        }
        print(capacityTester);
        capacityTester.clear();
        print(capacityTester);



        // tester/copier block
        ArrayClass tester = new ArrayClass(3, 1,69);
        print(tester);
        tester.appendItem(4);
        print(tester);
        tester.insertItemAt(1,5);
        System.out.println("Old Array");
        print(tester);

        ArrayClass copyTester = new ArrayClass(tester);
        System.out.println("copied Array");
        copyTester.resize(5);
        print(copyTester);
        copyTester.removeItemAt(4);
        print(copyTester);
        for(index=0;index<copyTester.getCurrentCapacity();index++)
        {
            copyTester.insertItemAt(index,index);
        }

        print(copyTester);

    }
    public static void print(ArrayClass printMe)
    {
        int index;
        for(index=0;index<printMe.getCurrentSize();index++)
        {
                System.out.print(printMe.accessItemAt(index)+" ");
        }
        System.out.println("");
    }

    public static void fullPrint(ArrayClass printMe)
    {
        int index;
        for(index=0;index<printMe.getCurrentCapacity();index++)
        {
                System.out.print(printMe.accessItemAt(index)+" ");
        }
        System.out.println("");
    }
}
