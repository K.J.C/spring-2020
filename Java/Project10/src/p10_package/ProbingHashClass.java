package p10_package;

public class ProbingHashClass
{
    /**
     Table size default
     */
    private final int DEFAULT_TABLE_SIZE =11;

    /**
     * Constant for returning item not found with search
     */
    public final int ITEM_NOT_FOUND =-1;

    /**
     * Constant for setting linear probing
     */
    public static final int LINEAR_PROBING=101;

    /**
     * Constant for setting quadratic probing
     */
    public static final int QUADRATIC_PROBING=102;

    /**
     * Size of the array table
     */
    private int tableSize;

    /**
     * Flag for setting linear or quadratic probing
     */
    private int probeFlag;

    /**
     * Array for hash table
     */
    private StudentClass[] tableArray;

    /**
     * Default constructor
     *
     * Initializes to default table size with probe flag set to linear probing
     */
    public ProbingHashClass()
    {
        tableSize=DEFAULT_TABLE_SIZE;
        probeFlag=LINEAR_PROBING;
        tableArray = new StudentClass[tableSize];
    }

    /**
     * Initialization constructor
     *
     * Initializes to default table size
     * with probe flag set to probe flag parameter
     * @param inProbeFlag sets linear or quadratic probing
     */
    public ProbingHashClass(int inProbeFlag)
    {
        tableSize=DEFAULT_TABLE_SIZE;
        probeFlag=inProbeFlag;
        tableArray = new StudentClass[tableSize];
    }

    /**
     *  Initialization constructor
     * @param inTableSize sets table size (capacity) but does not allow
     *                    table size to be less than default capacity
     * @param inProbeFlag sets linear or quadratic probing
     */
    public ProbingHashClass(int inTableSize, int inProbeFlag)
    {
        tableSize=inTableSize;
        probeFlag=inProbeFlag;
        tableArray = new StudentClass[tableSize];
    }

    /**
     * Copy constructor
     * @param copied ProbingHashClass object to be copied
     */
    public ProbingHashClass(ProbingHashClass copied)
    {
        tableSize=copied.tableSize;
        probeFlag=copied.probeFlag;
        tableArray= new StudentClass[tableSize];
        int index;
        for(index=0;index<tableSize;index++)
        {
            tableArray[index]=copied.tableArray[index];
        }
    }


    /**
     * Adds StudentClass item to hash table
     * <p>
     * Note: Uses hash index value from generateHash
     * <p>
     * Note: Shows probed index with data at the point of insertion
     * <p>
     * Note: Probe attempts are limited to the current size (capacity) of the table
     * @param newItem StudentClass item
     * @return Boolean success of operation
     */
    public boolean addItem(StudentClass newItem)
    {
        int newItemIndex=generateHash(newItem);
        int probeIndex;
        int powerBase=1;
        if(probeFlag == LINEAR_PROBING)
        {
            probeIndex=newItemIndex+1;
            while(probeIndex != newItemIndex)
            {
                if(probeIndex>=tableSize)
                {
                    probeIndex=probeIndex%tableSize;
                }
                if(tableArray[probeIndex]==null)
                {
                    tableArray[probeIndex]=newItem;
                    return true;
                }
                else if(probeIndex != newItemIndex)
                {
                    probeIndex++;
                }
            }
        }
        // Quadratic implied
        else
        {
            probeIndex=newItemIndex+toPower(powerBase,2);
            while(probeIndex != newItemIndex && powerBase < tableSize)
            {
                if(probeIndex>=tableSize)
                {
                    probeIndex=probeIndex%tableSize;
                }
                if(tableArray[probeIndex]==null)
                {
                    tableArray[probeIndex]=newItem;
                    return true;
                }
                else
                {
                    powerBase+=1;
                    probeIndex=newItemIndex+toPower(powerBase,2);
                }
            }
        }

        return false;
    }

    /**
     * Clears hash table by setting all bins to null
     */
    public void clearHashTable()
    {
        int indexToNullify;

        for(indexToNullify=0;indexToNullify<tableSize;indexToNullify++)
        {
            tableArray[indexToNullify]=null;
        }
    }

    /**
     * Returns item found
     * @param searchItem StudentClass value to be found; uses findItemIndex
     * @return StudentClass item found, or null if not found
     */
    public StudentClass findItem(StudentClass searchItem)
    {
       int findIndex=findItemIndex(searchItem);
       if(findIndex != ITEM_NOT_FOUND)
       {
           return tableArray[findIndex];
       }
       return null;
    }

    /**
     * Searches for item index in hash table
     * <p>
     * Note: Uses linear or quadratic probing as configured
     * <p>
     * Note: probing attempts limited to table size (capacity)
     * @param searchItem StudentClass value to be found
     * @return integer index location of search item
     */
    private int findItemIndex(StudentClass searchItem)
    {
        int searchItemIndex=generateHash(searchItem);
        int probeIndex;
        int powerBase=1;
        if(probeFlag == LINEAR_PROBING)
        {
            probeIndex=searchItemIndex+1;
            while(probeIndex != searchItemIndex)
            {
                if(probeIndex>=tableSize)
                {
                    probeIndex=probeIndex%tableSize;
                }
                if(tableArray[probeIndex] != null &&
                tableArray[probeIndex].compareTo(searchItem)==0)
                {
                    return probeIndex;
                }
                else
                {
                    probeIndex++;
                }
            }
        }
        else
        {
            probeIndex=searchItemIndex+toPower(powerBase,2);
            while(probeIndex != searchItemIndex && powerBase<tableSize)
            {
                if(probeIndex>=tableSize)
                {
                    probeIndex=probeIndex%tableSize;
                }
                if(tableArray[probeIndex] != null &&
                        tableArray[probeIndex].compareTo(searchItem)==0)
                {
                    return probeIndex;
                }
                else
                {
                    powerBase+=1;
                    probeIndex=searchItemIndex+toPower(powerBase,2);
                }
            }
        }

        return ITEM_NOT_FOUND;
    }

    /**
     * Method converts StudentClass hash value to index for use in hash table
     * @param item StudentClass value to be converted to hash value
     * Note: gets data from object via hashCode, then calculates index
     * <p>
     * Note: Uses hashCode from object
     * @return integer hash value
     */
    public int generateHash(StudentClass item)
    {
        int itemHash = item.hashCode();
        int itemIndex=(itemHash%tableSize);
        return itemIndex;
    }

    /**
     * Removes item from hash table
     * @param toBeRemoved StudentClass value used for
     *                    requesting data uses findItemIndex
     * @return StudentClass item removed, or null if not found
     */
    public StudentClass removeItem(StudentClass toBeRemoved)
    {
        StudentClass removedItem;
        int indexToBeRemoved=findItemIndex(toBeRemoved);
        if(indexToBeRemoved != ITEM_NOT_FOUND &&
                tableArray[indexToBeRemoved]!=null)
        {
            removedItem=tableArray[indexToBeRemoved];
            tableArray[indexToBeRemoved]=null;
            return removedItem;
        }
        return null;
    }

    /**
     * traverses through all array bins, finds min and max
     * number of contiguous elements, and number of empty nodes;
     * also shows table loading
     * <p>
     * NOTE: Generates string of used and unused bins in addition
     * to displaying results on screen
     * @return String result of hash table analysis
     */
    public String showHashTableStatus()
    {
        String outputString="";
        String binLayout="";
        int tableIndex;
        int emptyBins=0;
        int minBinStreak=tableSize;
        int maxBinStreak=0;
        int currentBinStreak=0;



        for(tableIndex=0;tableIndex<tableSize;tableIndex++)
        {
            if(tableArray[tableIndex]!= null)
            {
                binLayout+="D";
                currentBinStreak+=1;
            }
            if(tableArray[tableIndex]== null || tableIndex==tableSize-1)
            {
                binLayout+="N";
                emptyBins+=1;
                if(currentBinStreak<minBinStreak && currentBinStreak>0)
                {
                    minBinStreak=currentBinStreak;
                }
                if(currentBinStreak>maxBinStreak)
                {
                    maxBinStreak=currentBinStreak;
                }
                currentBinStreak=0;
            }
        }

        if(maxBinStreak<minBinStreak)
        {
            maxBinStreak=minBinStreak;
        }
        outputString+="Hash Table Status: "+ binLayout;
        outputString+="\n";
        outputString+="\n";
        outputString+="\tMinimum contiguous bins: "+ minBinStreak+"\n";
        outputString+="\tMaximum contiguous bins: "+ maxBinStreak+"\n";
        outputString+="\t\tNumber of empty bins:  "+ emptyBins+"\n";
        System.out.println(outputString);
        return binLayout;
    }


    /**
     * Local recursive method to calculate exponentiation with integers
     * @param base base of exponentiation
     * @param exponent exponent of exponentiation
     * @return result of exponentiation calculation
     */
    private static int toPower(int base, int exponent)
    {
        if(exponent == 0)
        {
            return 1;
        }
        else
        {
            return ((base*toPower(base,exponent-1)));
        }
    }


}
