package p5_package;

public class p5tester
{

    public static void main(String[] args)
    {
/*
        // STACK TESTING SECTION
        StackClass stackTester = new StackClass();
        System.out.println(stackTester.isFull());
        System.out.println(stackTester.isEmpty());
        System.out.println(stackTester.peek());
        stackTester.push(3);
        stackTester.push(2);
        System.out.println("PUSH 1 TO STACK SUCCESSFUL: "+stackTester.push(1));
        stackTester.push(3);
        stackTester.push(2);
        stackTester.push(1);
        stackTester.push(3);
        stackTester.push(2);
        stackTester.push(1);
        stackTester.push(3);
        stackTester.push(2);
        stackTester.push(1);
        stackTester.push(3);
        stackTester.push(2);
        System.out.println("PUSH 1 TO STACK SUCCESSFUL: "+stackTester.push(1));
        System.out.println("PEEK: "+stackTester.peek());
        stackTester.displayStack();
        System.out.println("STACK FULL: "+ stackTester.isFull());
        System.out.println("POP: "+ stackTester.pop());
        stackTester.displayStack();
        System.out.println("STACK CLEARED");
        stackTester.clear();
        System.out.println("STACK EMPTY: " +stackTester.isEmpty());
        stackTester.displayStack();
*/


        //ITERATOR TESTING SECTION.

        IteratorClass iterTester = new IteratorClass();
        iterTester.displayIteratorList();
        System.out.println(iterTester.addItem(1));
        System.out.println(iterTester.addItem(3));
        System.out.println(iterTester.addItem(5));
        System.out.println(iterTester.addItem(2));
        System.out.println(iterTester.addItem(1));
        System.out.println(iterTester.addItem(3));
        System.out.println(iterTester.addItem(5));
        System.out.println(iterTester.addItem(2));
        System.out.println(iterTester.addItem(1));
        System.out.println(iterTester.addItem(3));
        System.out.println("Array has all 10 slots filled");
        System.out.println(iterTester.addItem(5));
        System.out.println(iterTester.addItem(2));
        System.out.println(iterTester.addItem(3));
        System.out.println(iterTester.addItem(5));
        System.out.println(iterTester.addItem(2));
        System.out.println("Array is resized to have 20 slots");
        iterTester.resize(20);
        System.out.println(iterTester.addItem(10));
        System.out.println(iterTester.addItem(11));
        System.out.println(iterTester.addItem(54));
        System.out.println(iterTester.addItem(69));
        System.out.println(iterTester.addItem(420));

        System.out.println("--------------------------------");
        int index;

        for(index=0;index<16;index++)
        {
            iterTester.returnNext();
            iterTester.displayIteratorList();
        }
        System.out.println(iterTester.returnCurrent());
        iterTester.setToLastItem();
        System.out.println(iterTester.returnPrevious());
        iterTester.displayIteratorList();
        iterTester.clear();
        iterTester.setToFirstItem();
        iterTester.setToLastItem();
        iterTester.displayIteratorList();


    }

}
