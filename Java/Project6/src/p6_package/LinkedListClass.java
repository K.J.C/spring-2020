package p6_package;

/**
 *
 */
public class LinkedListClass
{
    /**
     * Reference to head of linked list
     */
    private BoxClass headRef;

    /**
     * Default constructor, initializes linked list to default capacity
     */
    public LinkedListClass()
    {
        headRef=null;
    }

    /**
     * Initializing constructor, fills all elements with specified size
     * value up to given size; overwrites any data already in list
     * @param size sets the number of items to be filled in linked list
     * @param fillValue value to be placed in all elements
     *                  of initialized linked list up to the size
     */
    public LinkedListClass(int size, BoxClass fillValue)
    {
        if(size>0)
        {
            headRef= new BoxClass(fillValue);
            int index;
            BoxClass refBox= new BoxClass(fillValue);
            headRef.nextRef=refBox;
            for(index=0;index<size-2;index++)
            {
                refBox.nextRef= new BoxClass(fillValue);
                refBox=refBox.nextRef;
            }
        }

    }

    /**
     * Copy constructor, initializes linked list to size and capacity of
     * copied linked list, then copies only the elements up to the given size
     * @param copied LinkedListClass object to be copied
     */
    public LinkedListClass(LinkedListClass copied)
    {
        // I really thought this would create aliasing but it does not seem to
        headRef= copied.headRef;
    }

    /**
     * Accesses item in linked list at specified index if index within linked list bounds
     * @param accessIndex index of requested element value
     * @return accessed value if successful, null if not
     */
    public BoxClass accessItemAt(int accessIndex)
    {
        if(accessIndex>getCurrentSize()-1 || accessIndex<0)
        {
            return null;
        }
        int index;
        BoxClass iteratorBox=headRef;
        for(index=0;index<accessIndex;index++)
        {
            iteratorBox=iteratorBox.nextRef;
        }
        return iteratorBox;
    }

    /**
     * Appends item to end of linked list, if linked list is not full,
     * <p>
     * e.g., no more values can be added
     * @param newValue BoxClass object to be appended to linked list
     */
    public void appendItem(BoxClass newValue)
    {
        if(headRef == null)
        {
            headRef=newValue;
            headRef.nextRef=null;
        }
        else
        {
            BoxClass tempBox = new BoxClass(newValue);
            tempBox.nextRef=null;
            findLastItemRef().nextRef=new BoxClass(tempBox);
        }
    }

    /**
     * Clears linked list of all valid values by setting linked list size to zero, values remain in linked list but are not accessible
     */
    public void clear()
    {
        headRef=null;
    }

    /**
     * Simple linked list dump for testing purposes
     */
    public void dump()
    {
        System.out.println("LinkedListClass Data Dump: ");
        int index=0;
        BoxClass iteratorBox=headRef;
        if(headRef != null)
        {
            while(index<getCurrentSize()-1)
            {
                if(iteratorBox.nextRef.equals(iteratorBox))
                {
                    System.out.println(index+". " + iteratorBox.toString());
                    index++;
                }
                else
                {
                    System.out.println(index+". " + iteratorBox.toString());
                    iteratorBox=iteratorBox.nextRef;
                    index++;
                }
            }
            System.out.println(index+". " + iteratorBox.toString());
        }
    }

    /**
     * Finds reference to last node in linked list
     * @return BoxClass reference to last item
     */
    private BoxClass findLastItemRef()
    {
        BoxClass iteratorBox=headRef;
        while(iteratorBox.nextRef != null)
        {
            iteratorBox= iteratorBox.nextRef;
        }
        return iteratorBox;
    }

    /**
     * Description: Gets current size of linked list
     * <p>
     * Note: size of linked list indicates number of valid
     * or viable values in the linked list
     * @return size of linked list
     */
    public int getCurrentSize()
    {
        //Hardcoded return of zero to prevent null pointer with an empty list
        if(headRef == null)
        {
            return 0;
        }
        return getCurrentSizeHelper(headRef);
    }

    /**
     * Helper method finds length of linked list
     * @param workingRef BoxClass reference used for recursion
     * @return integer value with size of linked list at a given
     * point in the recursion
     */
    public int getCurrentSizeHelper(BoxClass workingRef)
    {
        int recursionDepthValue=0;
        if(workingRef.nextRef!=null)
        {
                recursionDepthValue=getCurrentSizeHelper(workingRef.nextRef);
        }
        return recursionDepthValue+1;
    }

    /**
     * Description: Inserts item into linked list at specified index
     * <p>
     * Note: Value is inserted at given index which is inserted into the
     * linked list at that point
     * <p>
     * Note: Value can be inserted after the last valid element but
     * not at any index past that point
     * @param insertIndex index of element into which value is to be inserted
     * @param newValue value to be inserted into linked list
     * @return boolean success if inserted, or failure if linked list was full
     */
    public boolean insertItemAt(int insertIndex, BoxClass newValue)
    {
        if(insertIndex<0 || insertIndex>getCurrentSize())
        {
            return false;
        }
        BoxClass iteratorBox=headRef;
        int index=0;
        while(index<insertIndex-1)
        {
            iteratorBox= iteratorBox.nextRef;
            index++;
        }
        if(insertIndex==0)
        {
            BoxClass oldNext=iteratorBox;
            headRef=newValue;
            newValue.nextRef=oldNext;
            return true;
        }
        BoxClass oldNext=iteratorBox.nextRef;
        iteratorBox.nextRef=newValue;
        newValue.nextRef=oldNext;
        return true;
    }

    /**
     * Tests for size of linked list equal to zero,
     * no valid values stored in linked list
     * @return boolean result of test for empty
     */
    public boolean isEmpty()
    {
        return headRef==null;
    }

    /**
     * Tests for value found in object linked list;
     * returns true if value within linked list, false otherwise
     * @param testVal value to be tested
     * @return boolean true if value is found in linked list, false otherwise
     */
    public boolean isInlinkedList(BoxClass testVal)
    {
        int index;
        BoxClass iteratorBox = headRef;
        int currentSize=getCurrentSize();
        for(index=0;index<currentSize;index++)
        {

            if(iteratorBox.compareTo(testVal) == 0)
            {
                return true;
            }
            iteratorBox=iteratorBox.nextRef;
        }
        return false;
    }

    /**
     * Description: Removes item from linked list at specified index if
     * index within linked list size bounds
     * @param removeIndex index of element value to be removed
     * @return removed value if successful, null if not
     */
    public BoxClass removeItemAt(int removeIndex)
    {
        BoxClass iteratorBox=headRef;
        BoxClass returnBox;
        if(removeIndex<0 || removeIndex>getCurrentSize()-1)
        {
            return null;
        }

        if(removeIndex==0)
        {
            returnBox=headRef;
            headRef=headRef.nextRef;
            return returnBox;
        }
        int index=0;
        while(index<removeIndex-1)
        {
            iteratorBox= iteratorBox.nextRef;
            index++;
        }

        returnBox=iteratorBox.nextRef;
        iteratorBox.nextRef=iteratorBox.nextRef.nextRef;

        return returnBox;
    }


}
