package p6_package;

public class p6Tester
{
    public static void main(String[] args)
    {
        int index1=2;

        BoxClass aBox = new BoxClass(2,2);
        BoxClass bBox = new BoxClass(1,4);
        BoxClass cBox = new BoxClass(4,4);
        BoxClass dBox = new BoxClass(1,7);
        BoxClass eBox = new BoxClass(3,2);
        BoxClass fBox = new BoxClass(8,9);

        LinkedListClass defTester = new LinkedListClass();
        LinkedListClass fillTester = new LinkedListClass(4,fBox);
        System.out.println("fillTester Size: "+ fillTester.getCurrentSize());
        fillTester.dump();

        QueueClass queueTester = new QueueClass();

        queueTester.enqueue(aBox);
        queueTester.enqueue(bBox);
        queueTester.enqueue(cBox);
        queueTester.enqueue(dBox);
        System.out.println("Peeking Front: "+ queueTester.peekFront());
        System.out.println("Dequeuing: "+ queueTester.dequeue());
        queueTester.displayQueue();




        System.out.println("Size of empty list: "+ defTester.getCurrentSize());
        // This dump should do noting
        System.out.println("DUMPING EMTPY ARRAY\n");
        defTester.dump();

        System.out.println("Appending box A-D");
        defTester.appendItem(aBox);
        defTester.appendItem(bBox);
        defTester.appendItem(cBox);
        defTester.appendItem(dBox);

        System.out.println("\nDUMPING NOW");
        defTester.dump();

        // Should work fine.
        System.out.println("Appending extra aBox");
        defTester.appendItem(aBox);

        System.out.println("Inserting bBox at index 2");
        defTester.insertItemAt(2,bBox);

        System.out.println("\nDUMPING NOW");
        defTester.dump();

        System.out.println("Inserting box E at index "+index1);
        defTester.insertItemAt(index1,eBox);
        System.out.println("--------------------------");
        System.out.println("\nDUMPING NOW");
        defTester.dump();
        System.out.println("Nonexistent box present:" + defTester.isInlinkedList(fBox));
        System.out.println("Existent box present:" + defTester.isInlinkedList(cBox));


        System.out.println("Removing: "+ defTester.removeItemAt(3));
        System.out.println("\nDUMPING NOW");
        defTester.dump();
        System.out.println("Copying array with copy constructor");
        LinkedListClass copyTester = new LinkedListClass(defTester);
        System.out.println("CLEARING ORRIGANAL ARRAY");
        defTester.clear();
        System.out.println("\nDUMPING NOW(Original Array)");
        defTester.dump();

        System.out.println("\nDUMPING NOW(CopyTest)");
        copyTester.dump();

        System.out.println("Removing Item at index 3");
        copyTester.removeItemAt(3);

        System.out.println("\nDUMPING NOW(CopyTest)");
        copyTester.dump();

        System.out.println("\nDUMPING NOW(FillTest)");
        fillTester.dump();

        System.out.println("Accessing non existent index");
        System.out.println(fillTester.accessItemAt(1337));

        System.out.println("Accessing real index");
        System.out.println(copyTester.accessItemAt(3));

    }
}
