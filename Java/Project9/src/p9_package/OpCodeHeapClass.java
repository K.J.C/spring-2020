package p9_package;

/**
 * Array-based generic min heap class used as a priority queue for generic data
 */
public class OpCodeHeapClass
{
    /**
     * Initial array capacity
     */
    public final int DEFAULT_ARRAY_CAPACITY=10;
    /**
     * Array for heap
     */
    private OpCodeClass[] heapArray;
    /**
     * Management data for array
     */
    private int arraySize;
    /**
     * Management data for array
     */
    private int arrayCapacity;
    /**
     * Display flag can be set to observe bubble up and trickle down operations
     */
    private boolean displayFlag;

    /**
     * Default constructor sets up array management conditions and default display flag setting
     */
    public OpCodeHeapClass()
    {
        arrayCapacity=DEFAULT_ARRAY_CAPACITY;
        heapArray = new OpCodeClass[arrayCapacity];
        arraySize=0;
        displayFlag=false;
    }

    /**
     * Copy constructor copies array and array management conditions and default display flag setting
     * @param copied GenericHeapClass object to be copied
     */
    public OpCodeHeapClass(OpCodeHeapClass copied)
    {
        arrayCapacity=copied.arrayCapacity;
        arraySize=copied.arraySize;
        heapArray=copied.heapArray;
        displayFlag=copied.displayFlag;
    }

    /**
     * Accepts GenericData item and adds it to heap
     * <p>
     * Note: uses bubbleUpArrayHeap to resolve unbalanced heap after data addition
     * <p>
     * Note: must check for resize before attempting to add an item
     * @param newItem GenericData item to be added
     */
    public void addItem(OpCodeClass newItem)
    {
        if(displayFlag)
        {
            System.out.println("Adding new process: "+newItem);
        }
        checkForResize();
        heapArray[arraySize]=newItem;
        arraySize++;
        bubbleUpArrayHeap(arraySize-1);
    }

    /**
     * Recursive operation to reset data in the correct order for the min heap after new data addition
     * @param currentIndex currentIndex - index of current item being assessed, and moved up as needed
     */
    private void bubbleUpArrayHeap(int currentIndex)
    {
        OpCodeClass temp;
        boolean parentBiggerThanChild;
        int parentNodeIndex;
        // Even index
        if(currentIndex%2==0)
        {
            parentNodeIndex=(currentIndex/2)-1;
        }
        // Odd index
        else
        {
            parentNodeIndex=(currentIndex/2);
        }
        if(arraySize>1 && parentNodeIndex>=0)
        {
           parentBiggerThanChild= heapArray[parentNodeIndex].compareTo
                   (heapArray[currentIndex])>0;
            if(parentBiggerThanChild)
            {
                if(displayFlag)
                {
                    System.out.println("    - Bubble up:");
                    System.out.println("        - Swapping parent: "+
                            heapArray[parentNodeIndex]+"; with child:"+
                            heapArray[currentIndex]);
                }
                temp=heapArray[currentIndex];
                heapArray[currentIndex]=heapArray[parentNodeIndex];
                heapArray[parentNodeIndex]=temp;



                bubbleUpArrayHeap(parentNodeIndex);
            }

        }


    }

    /**
     * Automatic resize operation used prior to any new data addition in the heap
     *
     * Tests for full heap array, and resizes to twice the current capacity as required
     */
    private void checkForResize()
    {
        if(arraySize==arrayCapacity)
        {
            OpCodeClass[] newArray = new OpCodeClass[arrayCapacity*2];
            int index;
            for(index=0;index<arrayCapacity;index++)
            {
                newArray[index]=heapArray[index];
            }
            heapArray= newArray;
        }
    }

    /**
     * Tests for empty heap
     * @return boolean result of test
     */
    public boolean isEmpty()
    {
        return arraySize==0;
    }

    /**
     * Removes GenericData item from top of min heap, thus being the operation with the lowest priority value
     * <p>
     * Note: Uses trickleDownArrayHeap to resolve unbalanced heap after data removal
     * @return GenericData item removed
     */
    public OpCodeClass removeItem()
    {
        if(!isEmpty())
        {
            OpCodeClass returnedItem = heapArray[0];

            heapArray[0]=heapArray[arraySize-1];
            arraySize--;
            if(displayFlag)
            {
                System.out.println("Removing process: "+ returnedItem);
            }
            trickleDownArrayHeap(0);
            return returnedItem;
        }
        return null;
    }

    /**
     * Utility method to set the display flag for displaying internal operations of the heap bubble and trickle operations
     * @param setState flag used to set the state to display, or not;
     */
    public void setDisplayFlag(boolean setState)
    {
        displayFlag=setState;
    }

    /**
     * Dumps array to screen as is, no filtering or management
     */
    public void showArray()
    {
        int index;
        for(index=0;index<arraySize;index++)
        {
            System.out.println(heapArray[index]);
        }
    }

    /**
     * Recursive operation to reset data in the correct order for the min heap after data removal
     * @param currentIndex index of current item being assessed, and moved down as required
     */
    private void trickleDownArrayHeap(int currentIndex)
    {
        OpCodeClass temp;
            if(currentIndex<arraySize)
            {

                int leftNode=(currentIndex*2)+1;
                int rightNode=(currentIndex*2)+2;

                if(leftNode<arraySize &&
                        heapArray[currentIndex].compareTo
                                (heapArray[leftNode])>0)
                {
                    if(displayFlag)
                    {
                        System.out.println("  - Trickle down:");
                        System.out.println("     - Swapping Parent: "
                                + heapArray[currentIndex]+"; with left child:"
                                + heapArray[leftNode]);
                    }
                    temp=heapArray[currentIndex];
                    heapArray[currentIndex]=heapArray[leftNode];
                    heapArray[leftNode]=temp;
                    trickleDownArrayHeap(leftNode);
                }

                if(rightNode<arraySize &&
                        heapArray[currentIndex].compareTo
                                (heapArray[rightNode])>0)
                {
                    if(displayFlag)
                    {
                        System.out.println("  - Trickle down:");
                        System.out.println("     - Swapping Parent: "
                                + heapArray[currentIndex]+"; with right child:"
                                + heapArray[rightNode]);
                    }
                    temp=heapArray[currentIndex];
                    heapArray[currentIndex]=heapArray[rightNode];
                    heapArray[rightNode]=temp;
                    trickleDownArrayHeap(rightNode);
                }


            }

    }
}

