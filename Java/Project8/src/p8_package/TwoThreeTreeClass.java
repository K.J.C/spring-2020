package p8_package;

public class TwoThreeTreeClass
{


    /**
     * root of tree
     */
    private TwoThreeNodeClass root;

    /**
     * constant used for identifying one data item stored
     */
    private final int ONE_DATA_ITEM=1;

    /**
     * constant used for identifying two data items stored
     */
    private final int TWO_DATA_ITEMS=2;
    /**
     * constant used for identifying three data items stored
     */
    private final int THREE_DATA_ITEMS=3;

    /**
     * Used for acquiring ordered tree visitations in String form
     */
    private String 	outputString;

    /**
     *  Default 2-3 tree constructor
     */
    public TwoThreeTreeClass()
    {
        root=null;
    }

    /**
     * Copy 2-3 tree constructor
     * @param copied TwoThreeTreeClass object to be duplicated; data is copied,
     *              but new nodes and references must be created
     */
    public TwoThreeTreeClass(TwoThreeTreeClass copied)
    {
        if(copied.root != null)
        {
            copyConstructorHelper(copied.root);
        }
        else
        {
            root=null;
        }
    }

    /**
     * Implements tree duplication effort with recursive method;
     * copies data into newly created nodes and
     * creates all new references to child nodes
     * @param workingCopiedRef TwoThreeNodeClass reference that is updated to
     *                        lower level children with each recursive call
     * @return TwoThreeTreeClass object to be duplicated; data is copied,
     * but new nodes and references must be created
    TwoThreeNodeClass reference to next higher level node;
    last return is to root node of THIS object
     */
    private TwoThreeNodeClass copyConstructorHelper
    (TwoThreeNodeClass workingCopiedRef)
    {
        if(workingCopiedRef != null)
        {
            addItem(workingCopiedRef.leftData);
            addItem(workingCopiedRef.centerData);
            addItem(workingCopiedRef.rightData);
            if(workingCopiedRef.leftChildRef != null)
            {
                return copyConstructorHelper(workingCopiedRef.leftChildRef);
            }
            if(workingCopiedRef.centerChildRef != null)
            {
                return  copyConstructorHelper(workingCopiedRef.centerChildRef);
            }
            if(workingCopiedRef.rightChildRef != null)
            {
                return  copyConstructorHelper(workingCopiedRef.rightChildRef);
            }
        }
        return null;
    }

    /**
     * Method is called when addItemHelper arrives at the bottom of the 2-3
     * search tree
     * <p>
     * Assumes one- or two- value nodes and adds one more to the
     * appropriate one resulting in a two- or three- value node
     * (i.e., all node's children are null);
     * @param localRef TwoThreeNodeClass reference has original node data and c
     *                 ontains added value when method completes; method does
     *                 not manage any node links
     * @param itemVal integer value to be added to 2-3 node
     */
    private void addAndOrganizeData(TwoThreeNodeClass localRef, int itemVal)
    {
        TwoThreeNodeClass workingNode = new TwoThreeNodeClass(itemVal);
        if(localRef.centerData == 0)
        {
            localRef.centerData= itemVal;
        }
        else if(itemVal < localRef.centerData)
        {
            if(localRef.leftData != 0)
            {
                localRef.rightData=localRef.centerData;
                localRef.centerData = itemVal;
            }
            else
            {
                localRef.leftData=itemVal;
            }

        }
        else if(itemVal > localRef.centerData)
        {
            localRef.rightData = itemVal;
        }
        fixUpInsert(localRef);

    }

    /**
     * Adds item to 2-3 tree using addItemHelper as needed
     * @param itemVal integer value to be added to the tree
     */
    public void addItem(int itemVal)
    {
        addItemHelper(root,itemVal);
    }

    /**
     * Helper method searches from top of tree to bottom using
     * divide and conquer strategy to find correct location (node)
     * for new added value; once location is found, item is added to node
     * using addAndOrganizeData and then fixUpInsert is called in case the
     * updated node has become a three-value node
     *
     * @param localRef TwoThreeNodeClass reference to the current item at
     *                the same given point in the recursion process
     * @param itemVal integer value to be added to the tree
     */
    // NOTE: 0 is a reserved value for empty spaces due to its status as
    // Default value for primitive int do not attempt to invoke this method with
    // itemVal equal to 0
    private void addItemHelper(TwoThreeNodeClass localRef, int itemVal)
    {
        TwoThreeNodeClass toBeAdded= new TwoThreeNodeClass(itemVal);
        if(root==null)
        {
            root = new TwoThreeNodeClass(itemVal);
            localRef=root;
        }


        else
        {
            if(localRef.leftData !=0 && itemVal < localRef.leftData)
            {
                if(localRef.leftChildRef == null)
                {
                    localRef.leftChildRef = new TwoThreeNodeClass(itemVal);
                }
                addItemHelper(localRef.leftChildRef,itemVal);
            }

            if(localRef.leftData !=0 &&
                    localRef.rightData != 0 &&
                    itemVal > localRef.leftData &&
                    itemVal < localRef.rightData)
            {
                if(localRef.centerChildRef == null)
                {
                    localRef.centerChildRef = new TwoThreeNodeClass(itemVal);
                }
                addItemHelper(localRef.leftChildRef,itemVal);
            }
            else if(localRef.rightData != 0)
            {
                if(localRef.rightChildRef == null)
                {
                    localRef.rightChildRef = new TwoThreeNodeClass(itemVal);
                }
                addItemHelper(localRef.rightChildRef,itemVal);
            }
        }
        if(localRef.centerChildRef == null &&
                localRef.leftChildRef == null &&
                localRef.rightChildRef == null)
        {
            addAndOrganizeData(localRef,itemVal);
        }

    }

    /**
     *  Method clears tree so that new items can be added again
     */
    public void clear()
    {
        root=null;
    }

    /**
     * Method used to fix tree any time a three-value node has been
     * added to the bottom of the tree; it is always called but decides
     * to act only if it finds a three-value node
     * <p>
     * Resolves current three-value node which may add a value to
     * the node above; if the node above becomes a three-value node,
     * then this is resolved with the next recursive call
     * <p>
     * Recursively climbs from bottom to top of tree resolving
     * any three-value nodes found
     *
     * @param localRef TwoThreeNodeClas reference initially given the currently
     *                updated node, then is given parent node recursively
     *                 each time a three-value node was resolved
     */
    private void fixUpInsert(TwoThreeNodeClass localRef)
    {

        int tempVar;
        if(localRef.numItems > THREE_DATA_ITEMS)
        {
            if (localRef.leftData == 0 &&
                    localRef.centerData == 0 &&
                    localRef.leftData > localRef.centerData)
            {
                tempVar = localRef.centerData;
                localRef.centerData = localRef.leftData;
                localRef.leftData = tempVar;
            }
            if (localRef.rightData != 0 &&
                    localRef.centerData != 0 &&
                    localRef.rightData < localRef.centerData)
            {
                tempVar = localRef.centerData;
                localRef.centerData = localRef.rightData;
                localRef.rightData = tempVar;
            }

            if (localRef.leftData != 0)
            {
                localRef.leftChildRef = new
                        TwoThreeNodeClass(TwoThreeNodeClass.LEFT_CHILD_SELECT
                        ,localRef);
                localRef.leftChildRef.parentRef = localRef;
                localRef.leftData = 0;
            }

            if (localRef.rightData != 0)
            {
                localRef.rightChildRef = new TwoThreeNodeClass(localRef);
                localRef.rightChildRef.parentRef = localRef;
                localRef.rightData = 0;
            }
        }
        else if(localRef.parentRef != null)
        {
            fixUpInsert(localRef.parentRef);
        }


    }



    /**
     * Tests center value if single node, tests left and right values if
     * two-value node; returns true if specified data is found in any given node
     * <p>
     * Note: Method does not use any branching operations such as if/else/etc.
     * @param localRef TwoThreeNodeClass reference to node with one
     *                or two data items in it
     * @param searchVal integer value to be found in given node
     * @return boolean result of test
     */
    private boolean foundInNode(TwoThreeNodeClass localRef, int searchVal)
    {
        return localRef.leftData == searchVal ||
                localRef.centerData == searchVal ||
                localRef.rightData == searchVal;
    }

    /**
     * Public method called by user to display data in order
     * @return String result to be displayed and/or analyzed
     */
    public String inOrderTraversal()
    {

        if(root != null)
        {
            inOrderTraversalHelper(root);
        }
        return outputString;
    }

    /**
     * Helper method conducts in order traversal with 2-3 tree
     * <p>
     * Shows location of each value in a node: "C" at center of single node
     * "L" or "R" at left or right of two-value node
     * @param localRef TwoThreeNodeClass reference to current location
     *                 at any given point in the recursion process
     */
    private void inOrderTraversalHelper(TwoThreeNodeClass localRef)
    {
        // Welcome to if land I'll be your tour guide
        outputString="";
        if(localRef.leftChildRef != null)
        {
            inOrderTraversalHelper(localRef.leftChildRef);
        }
        if(localRef.leftData != 0)
        {
            outputString+="L"+localRef.leftData+" ";
        }
        if(localRef.centerChildRef != null)
        {
            inOrderTraversalHelper(localRef.centerChildRef);

        }
        if(localRef.centerData != 0)
        {
            outputString+="C"+localRef.centerData+" ";
        }
        if(localRef.rightChildRef != null)
        {
            inOrderTraversalHelper(localRef.rightChildRef);
        }
        if(localRef.rightData != 0)
        {
            outputString+="R"+localRef.rightData+" ";
        }




    }

    /**
     * Search method used by programmer to find specified item in 2-3 tree
     * @param searchVal integer value to be found
     * @return boolean result of search effort
     */
    public boolean search(int searchVal)
    {
        if(root != null)
        {
            return searchHelper(root,searchVal);
        }
        return false;
    }

    /**
     * Search helper method that traverses through tree in a
     * recursive divide and conquer search fashion to find given
     * integer in 2-3 tree
     *
     * @param localRef TwoThreeNodeClass reference to given node at any
     *                point during the recursive process
     * @param searchVal integer value to be found in tree
     * @return   boolean result of search effort
     */
    private boolean searchHelper(TwoThreeNodeClass localRef, int searchVal)
    {
        if(foundInNode(localRef,searchVal))
        {
            return true;
        }
        if(localRef.leftData != 0 && searchVal < localRef.leftData)
        {
            return searchHelper(localRef.leftChildRef,searchVal);
        }
        // center Child path
        if(localRef.centerData != 0 && searchVal > localRef.leftData &&
                searchVal < localRef.rightData)
        {
            return searchHelper(localRef.leftChildRef,searchVal);
        }
        // Right Child path
        if(localRef.rightData != 0 && searchVal > localRef.rightData)
        {
            return searchHelper(localRef.leftChildRef,searchVal);
        }

        return false;
    }
}
