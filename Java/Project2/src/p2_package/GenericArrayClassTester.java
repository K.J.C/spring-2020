package p2_package;

public class GenericArrayClassTester {

    public static void main(String[] args)
    {
        StudentClass genericStudent = new StudentClass();

        StudentClass student1 = new StudentClass("AAA",1111,'F',1.0);
        StudentClass student2 = new StudentClass("CCC",2222 ,'M',2.0);
        StudentClass student3 = new StudentClass("ZZZ",3333 ,'F',3.0);
        StudentClass student4=  new StudentClass("GGG",4444 ,'M',4.0);
        StudentClass student5 = new StudentClass("111",4363,'F',1.0);
        StudentClass student6 = new StudentClass("666",6346 ,'M',2.0);
        StudentClass student7 = new StudentClass("420",7457 ,'F',3.0);
        StudentClass student8= new StudentClass("699",1234 ,'M',4.0);

        // Alt Sort Key Section

        int sortOrder=0;
        student1.setSortDirKey(sortOrder);
        student2.setSortDirKey(sortOrder);
        student3.setSortDirKey(sortOrder);
        student4.setSortDirKey(sortOrder);
        student5.setSortDirKey(sortOrder);
        student6.setSortDirKey(sortOrder);
        student7.setSortDirKey(sortOrder);
        student8.setSortDirKey(sortOrder);

        int sortChoice=65465;
        student1.setSortKey(sortChoice);
        student2.setSortKey(sortChoice);
        student3.setSortKey(sortChoice);
        student4.setSortKey(sortChoice);
        student5.setSortKey(sortChoice);
        student6.setSortKey(sortChoice);
        student7.setSortKey(sortChoice);
        student8.setSortKey(sortChoice);



        GenericArrayClass tester = new GenericArrayClass(2);
        GenericArrayClass defaultTester = new GenericArrayClass();

        System.out.println(printData(tester)+"----------------------");
        tester.resize(10);

        tester.appendItem(student1);
        tester.appendItem(student2);

        System.out.println(printData(tester)+"----------------------");
        tester.insertItemAt(0 ,student3);
        tester.insertItemAt(1,student4);
        tester.insertItemAt(2,student5);
        tester.insertItemAt(3,genericStudent);
        tester.insertItemAt(2,student6);
        tester.insertItemAt(4,student7);
        tester.insertItemAt(4,student8);


        int index;
        for(index=0;index<10;index++)
        {
            defaultTester.appendItem(genericStudent);
        }

        System.out.println(printData(tester)+"----------------------");
       System.out.println("Removed: "+tester.removeItemAt(3));

        System.out.println(printData(tester)+"----------------------");

        // System.out.println("Running Selection Sort");
        // tester.runSelectionSort();
       //  System.out.println("Running Insertion Sort");
       //  tester.runInsertionSort();
         System.out.println("Running Bubble Sort");
         tester.runBubbleSort();
        //System.out.println("Running Shell Sort");
       // tester.runShellSort();

        System.out.println(printData(tester)+"----------------------");
     //   defaultTester.clear();
     //   System.out.println(printData(defaultTester));
    }

    public static String printData(GenericArrayClass input)
    {
        int index=0;
        String output="";
        while(index<input.getCurrentCapacity())
        {
            if(input.accessItemAt(index)!=null)
            {
                output+=input.accessItemAt(index) + "\n";
            }
            index++;
        }
        return output;
    }



}
