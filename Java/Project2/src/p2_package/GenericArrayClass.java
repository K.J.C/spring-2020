package p2_package;

/**
 * Description: Class wrapper for a Java array,
 * with additional management operations.
 * Capable of processing any data complying with the accompanying interface
 * <p>
 * Note: Maintains a capacity value for maximum number of
 * items that can be stored,
 * and a size value for the number of valid or viable data items in the array
 *
 * @author  Kyler Carling
 * @version 1.1
 * @since   Jan 28 2020
 */

public class GenericArrayClass <GenericData extends Comparable<GenericData>>
{

    private int arrayCapacity;
    private int arraySize;
    private final static int DEFAULT_CAPACITY=10;
    private Object[] localArray;

    /**
     * Initializing constructor, initializes array to specified capacity
     */
    public GenericArrayClass()
    {
    arrayCapacity=DEFAULT_CAPACITY;
    arraySize=0;
    localArray = new Object[DEFAULT_CAPACITY];
    }

    /**
     * Initializing constructor, initializes array to specified capacity
     * @param capacity maximum capacity specification for the array
     */
    public GenericArrayClass(int capacity)
    {
        arrayCapacity=capacity;
        arraySize=0;
        localArray= new Object[capacity];
    }

    /**
     * Accesses item in array at specified index
     * if index within array size bounds
     * @param accessIndex index of requested element value
     * @return accessed value if successful, null if not
     */
    public GenericData accessItemAt(int accessIndex)
    {
        if(accessIndex>=0 && accessIndex<arraySize)
        {
            return (GenericData) localArray[accessIndex];
        }
        return null;

    }

    /**
     * Appends item to end of array, if array is not full,*
     * e.g., no more values can be added
     * @param newValue value to be appended to array
     * @return Boolean success if appended, or failure if array was full
     */
    public boolean appendItem(GenericData newValue)
    {
        if(isFull())
        {
            return false;
        }
        arraySize++;
        localArray[arraySize-1]=newValue;
        return true;
    }

    /**
     * Clears array of all valid values by setting array size to zero,
     * values remain in array but are not accessible
     */
    public void clear()
    {
        arraySize=0;
    }

    /**
     * Description: Gets current capacity of array
     * <p>
     * Note: capacity of array indicates number
     * of values the array can hold
     * @return capacity of array
     */
    public int getCurrentCapacity()
    {
        return arrayCapacity;
    }

    /**
     * Description: Gets current size of array
     * <p>
     * Note: size of array indicates number of valid or viable values
     * in the array
     * @return size of array
     */
    public int getCurrentSize()
    {
        return arraySize;
    }

    /**
     * Description: Inserts item to array at specified index
     * if array is not full, e.g., no more values can be added
     * @return  Boolean success if inserted, or failure if array was full
     */
    public boolean insertItemAt(int insertIndex,GenericData newValue)
    {
        //Gatekeeper block to throw out bad input
        if(isFull() || insertIndex>arraySize+1 || insertIndex<0)
        {
            return false;
        }
        arraySize++;

        // -1 offset to convert from size value to array index
        int positionIndex=arraySize-1;
        while (positionIndex>=insertIndex && !isFull())
        {
            localArray[positionIndex+1]=localArray[positionIndex];
            positionIndex--;
        }
        localArray[insertIndex]=newValue;
        return true;
    }

    /**
     * Tests for size of array equal to zero, no valid values stored in array
     * @return Boolean result of test for empty
     */
    public boolean isEmpty()
    {
        return arraySize == 0;
    }

    /**
     * Tests for size of array equal to capacity, no more values can be added
     * @return Boolean result of test for full
     */
    public boolean isFull()
    {
        return arraySize==arrayCapacity;
    }

    /**
     * Description: Removes item from array at specified index
     * if index within array size bounds
     * <p>
     * Note: Each data item from the element immediately above the remove index
     * to the end of the array is moved down by one element
     * @param removeIndex index of element value to be removed
     * @return removed value if successful, null if not
     */

    public GenericData removeItemAt(int removeIndex)
    {
        //Gatekeeper block to throw out bad input
        if(isEmpty() || removeIndex<0 || removeIndex>arraySize-1)
        {
            return null;
        }

        GenericData indexToDelete=accessItemAt(removeIndex);
        int oldPositionIndex=removeIndex;
        int newPositionIndex=oldPositionIndex+1;
        while(oldPositionIndex<arrayCapacity-1)
        {
            localArray[oldPositionIndex]=localArray[newPositionIndex];
            oldPositionIndex++;
            newPositionIndex++;
        }
        arraySize--;
        return indexToDelete;
    }

    /**
     * Description: Resets array capacity, copies current size and current
     * size number of elements
     * <p>
     * Exception: Method will not resize capacity below current array size,
     * returns false if this is attempted, true otherwise
     * @param newCapacity new capacity to be set; must be larger than
     * current capacity
     * @return Boolean condition of resize success or failure
     */
    boolean resize(int newCapacity)
    {
        if(newCapacity<=arrayCapacity)
        {
            return false;
        }

        Object[] resizedArr = new Object[newCapacity+1];
        int arrayIterator=0;
        while(arrayIterator<=arraySize)
        {
            resizedArr[arrayIterator]=accessItemAt(arrayIterator);
            arrayIterator++;
        }
        arrayCapacity=newCapacity+1;

        this.localArray=resizedArr;
        return true;
    }

    /**
     *  Description: Sorts elements using the bubble sort algorithm
     *  <p>
     *  Note: The data is sorted using the compareTo method of the given
     *  data set; the compareTo method decides the key and the order resulting
     */
    public void runBubbleSort()
    {
        int index=0;
        GenericData currentStudent;
        GenericData nextStudent;
        while(index<arraySize)
        {
            currentStudent=accessItemAt(index);
            nextStudent=accessItemAt(index+1);
            if(nextStudent != null && currentStudent.compareTo(nextStudent)>0)
            {
                swapElements(index+1,index);
                runBubbleSort();
            }
            index++;
        }
    }

    /**
     * Description: Sorts elements using the insertion sort algorithm
     * <p>
     * Note: The data is sorted using the compareTo method of the
     * given data set; the compareTo method decides the key and the
     * order resulting
     */
    public void runInsertionSort()
    {
        int arrayIndex=0;
        int sortedIndex=0;

        while(sortedIndex<arraySize)
        {
            arrayIndex=sortedIndex;
            while(arrayIndex<arraySize && accessItemAt(arrayIndex)!=null)
            {
                if(accessItemAt(arrayIndex).compareTo
                        (accessItemAt(sortedIndex))<0)
                {
                    GenericData varibleToInsert=
                            (GenericData) localArray[arrayIndex];
                    removeItemAt(arrayIndex);
                    insertItemAt(sortedIndex, varibleToInsert);
                }
                arrayIndex++;
            }
                sortedIndex++;
        }
    }


    /**
     * Description: Sorts elements using the selection sort algorithm
     * <p>
     *  Note: The data is sorted using the compareTo method of the given
     *  data set; the compareTo method decides the key and the order resulting
     */
    public void runSelectionSort()
    {
        int arrayIndex;
        int lowestIndex=0;
        int sortedIndex=0;
        boolean foundLowerValue=false;
        GenericData lowest=accessItemAt(0);

        while(sortedIndex<arraySize)
        {
            arrayIndex=sortedIndex;
            while(arrayIndex<arraySize-1)
            {
                if(accessItemAt(arrayIndex).compareTo(lowest)<0)
                {
                    foundLowerValue=true;
                    lowest=accessItemAt(arrayIndex);
                    lowestIndex=arrayIndex;
                }
                arrayIndex++;
            }
            // 2 offset to account for both size to index conversation and
            // prevent incorrect swap in an edge case
            if(foundLowerValue)
            {
                swapElements(sortedIndex,lowestIndex);
                foundLowerValue=false;
            }
            sortedIndex++;
            lowest=accessItemAt(sortedIndex);
        }
    }

    /**
     * Uses Shell's sorting algorithm to sort an array of integers
     * <p>
     * Shell's sorting algorithm is an optimized insertion algorithm
     *
     * <p>
     * Note: Creates new StudentClass array, sorts contents of array,
     * and returns the sorted result;
     * does not modify (this) object student array
     *
     * @return new StudentClass array with sorted items
     */
    @SuppressWarnings( "unchecked" )
    public void runShellSort()
    {
        int gap, gapPassIndex, insertionIndex;
        GenericData tempItem, testItem;
        boolean continueSearch;

        for( gap = arraySize / 2; gap > 0; gap /= 2 )
        {
            for( gapPassIndex = gap;
                 gapPassIndex < arraySize; gapPassIndex++ )
            {
                tempItem = (GenericData)localArray[ gapPassIndex ];

                insertionIndex = gapPassIndex;

                continueSearch = true;

                while( continueSearch && insertionIndex >= gap )
                {
                    testItem
                            = (GenericData)localArray[ insertionIndex - gap ];

                    if(tempItem!=null && testItem.compareTo( tempItem ) >  0)
                    {
                        localArray[ insertionIndex ]
                                = localArray[ insertionIndex - gap ];

                        insertionIndex -= gap;
                    }

                    else
                    {
                        continueSearch = false;
                    }

                }  // end search loop

                localArray[ insertionIndex ] = tempItem;
            }  // end list loop

        }  // end gap size setting loop

    }

    /**
     * Swaps one element in the local array at a given index
     * with another element in the array at the other given element
     *
     * @param oneIndex index of one of two elements to be swapped
     *
     * @param otherIndex index of second of two elements to be swapped
     */
    private void swapElements( int oneIndex, int otherIndex )
    {
        Object temp = localArray[ oneIndex ];
        localArray[ oneIndex ] = localArray[ otherIndex ];
        localArray[ otherIndex ] = temp;
    }

}