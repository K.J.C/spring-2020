package p7_package;


import java.io.*;

/** Provides workbench for testing Generic_BST_Class
 * 
 * @author MichaelL
 *
 */
public class BST_ClassMain
   {
    private final static int END_OF_FILE_MARKER = -1;

    // used for acquiring data via several methods
    private static FileReader fileIn;
    
      /** main method for driving multiple tests on Generic BST Class
       * 
       * @param args command-line string input arguments
       */
      public static void main(String[] args)
         {
          Generic_BST_Class<StudentClass> gSC = new Generic_BST_Class<>();
          String fileName = "in_5_Tabbed.txt";
          StudentClass temp;
          StudentClass student1 = new StudentClass("Dallas",1111,'M',0.0);
          StudentClass student2 = new StudentClass("Chains",2222 ,'M',0.0);
          StudentClass student3 = new StudentClass("Houston",3333 ,'M',0.0);
          StudentClass student4=  new StudentClass("Wolf",4444 ,'M',0.0);
          StudentClass student5=  new StudentClass("Hoxton",5555 ,'M',0.0);
          StudentClass student6=  new StudentClass("Wick",6666 ,'M',0.0);
          StudentClass student7=  new StudentClass("Bodhi",7777 ,'M',0.0);
          StudentClass student8=  new StudentClass("Dragan",8888 ,'M',0.0);
          StudentClass student9=  new StudentClass("Clover",9999 ,'M',0.0);
          StudentClass student10=  new StudentClass("Jacket",0000 ,'M',0.0);
          StudentClass student11=  new StudentClass("Bain",1337 ,'M',0.0);



          // title
          System.out.println( "\nGeneric_BST_Class Data Testing Program\n");
          
          // access data from file
             // test initialization constructor, appendItem, resize 
          System.out.println( "Data Retrieval from file - Begin");
       //   gSC = getData( fileName );
          System.out.println( "Data Retrieval from file - End");
          System.out.print( "\nData Display - In Order \n" );
          gSC.displayInOrder();
          
          // test for addition of redundant data
          // ---> Insert code here
             gSC.insert(student1);
             gSC.insert(student2);
             gSC.insert(student3);
             gSC.insert(student4);
             gSC.insert(student5);
             gSC.insert(student6);
             gSC.insert(student7);
             gSC.insert(student8);
             gSC.insert(student9);
             gSC.insert(student10);
          // ---> Display code here
          gSC.displayInOrder();
          // test for removal of data

          gSC.clearTree();
          System.out.print( "\nData Display after clearing tree - \n" );
          gSC.displayInOrder();

  //        gSC = getData( fileName );
             gSC.insert(student1);
             gSC.insert(student2);
             gSC.insert(student3);
             gSC.insert(student4);
             gSC.insert(student5);
             gSC.insert(student6);
             gSC.insert(student7);
             gSC.insert(student8);
             gSC.insert(student9);
             gSC.insert(student10);


          System.out.print( "\nData Display after reload Post Order - \n" );
          gSC.displayPostOrder();

          System.out.print( "\nData Display after reload Pre Order - \n" );
          gSC.displayPreOrder();
        System.out.println("-------------------------------------------");
             System.out.print( "\nData Display before removal In Order - \n" );
             gSC.displayInOrder();
             System.out.println("-------------------------------------------");
             System.out.println("Searched Item: "+ gSC.search(student1));

             /*
          StudentClass student1 = new StudentClass("Dallas",1111,'M',0.0);
          StudentClass student2 = new StudentClass("Chains",2222 ,'M',0.0);
          StudentClass student3 = new StudentClass("Houston",3333 ,'M',0.0);
          StudentClass student4=  new StudentClass("Wolf",4444 ,'M',0.0);
          StudentClass student5=  new StudentClass("Hoxton",5555 ,'M',0.0);
          StudentClass student6=  new StudentClass("Wick",6666 ,'M',0.0);
          StudentClass student7=  new StudentClass("Bodhi",7777 ,'M',0.0);
          StudentClass student8=  new StudentClass("Dragan",8888 ,'M',0.0);
          StudentClass student9=  new StudentClass("Clover",9999 ,'M',0.0);
          StudentClass student10=  new StudentClass("Jacket",0000 ,'M',0.0);
          StudentClass student11=  new StudentClass("Bain",1337 ,'M',0.0);
              */
            System.out.println("Removed Item: " +gSC.removeItem(student3)+"\n") ;
             gSC.displayInOrder();
             System.out.println("-----------------------------");

             System.out.println("Searched Item: "+ gSC.search(student1));
             System.out.println("Searched Item: "+ gSC.search(student2));
             System.out.println("Searched Item: "+ gSC.search(student3));
             System.out.println("Searched Item: "+ gSC.search(student4));
             System.out.println("Searched Item: "+ gSC.search(student5));
             System.out.println("Searched Item: "+ gSC.search(student6));
             System.out.println("Searched Item: "+ gSC.search(student7));
             System.out.println("Searched Item: "+ gSC.search(student8));
             System.out.println("Searched Item: "+ gSC.search(student9));
             System.out.println("Searched Item: "+ gSC.search(student10));
             System.out.println("Searched Item: "+ gSC.search(student11));


      //       gSC.removeItem(student2);
        //     gSC.displayInOrder();
/*
          // test for removal from left node
             temp = gSC.removeItem(student1);
          // ---> Remove code here
          System.out.print( 
               "\nData Display after removal from left node: " + temp + " - \n" );
          gSC.displayInOrder();
          
          // test for removal from right node
          // ---> Insert code here
          // ---> Remove code here
          gSC.displayInOrder();
          
          // test for removal from node having no children
          // ---> Remove code here
          System.out.print( 
 "\nData Display after removal from node having no children: " + temp + " - " );
          gSC.displayInOrder();

          // test for removal from node having two children
          // ---> Remove code here
          System.out.print( 
 "\nData Display after removal from node having two children: " + temp + " - ");
          gSC.displayInOrder();

          // test for removal from node having two children
          // ---> Remove code here
          System.out.print( 
 "\nData Display after attempt to remove value not in tree: >" + temp + "< - ");
          gSC.displayInOrder();
*/
          System.out.println( "\n --- End of Program--- " );


         }
      
      /**
       * Local method uploads data character by character,
       * parses characters, and loads into StudentClass
       * type data
       * <p>
       * Exception: If there is a file failure such as file not found,
       * method will return null
       * 
       * @param fileName name of file in local directory required for upload
       * 
       * @return returns Generic_BST_Class object holding StudentClass data
       */
      public static Generic_BST_Class<StudentClass> getData( String fileName )
         {
          int value;

          StudentClass studentClassObject;
          Generic_BST_Class<StudentClass> bstClassObject 
                    = new Generic_BST_Class<StudentClass>();
          String nameStr, idStr, gpaStr;
          int idVal;
          char genderVal;
          double gpaVal;
          boolean failedAccess = false;

          //FileReader 
          fileIn = null;
          
          try
             {
              fileIn = new FileReader( fileName );
              
              // read prime, name
              value = fileIn.read();

              while( value != END_OF_FILE_MARKER && !failedAccess )
                 {
                  // reset input strings
                  nameStr = ""; idStr = ""; gpaStr = "";

                    // get name
                  while( value != ';')
                     {
                      nameStr += (char)value;
                      
                      value = fileIn.read();
                     }
                  
                  // skip spaces up to integer value characters
                  value = getNextCharInt( "0123456789" );
                  
                  // get id
                  while( value >= '0' && value <= '9' )
                     {
                      idStr += (char)value;
                      
                      value = fileIn.read();
                     }
                  
                  // translate id
                  idVal = Integer.parseInt( idStr );

                  // skip spaces up to Mm or Ff
                  value = getNextCharInt( "MFmf" );

                  // load gender
                  genderVal = (char)value;
                  
                  // skip empty spaces up to double value characters
                  value = getNextCharInt( "0123456789." );

                  // get gpa
                  while( ( value >= '0' && value <= '9' ) || value == '.' )
                     {
                      gpaStr += (char)value;
                      
                      value = fileIn.read();
                     }
                  
                  // translate gpa
                  gpaVal = Double.parseDouble( gpaStr );                  

                  // load data into StudentClass object
                  studentClassObject = new StudentClass( nameStr, idVal, 
                                                         genderVal, gpaVal );
                  
                  // load StudentClass object into BST
                  bstClassObject.insert( studentClassObject );
                  
                  // skip end of line, etc., up to next letter 
                  // (name on next line)             
                  value = getNextCharInt( 
                    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" );
                  
                 }  // end data collection loop
              
              if( fileIn != null )
                 {
                  fileIn.close();
                 }
             }
          
          catch( IOException ioe )
             {
              failedAccess = true;
             }
         
          if( failedAccess )
             {
              bstClassObject = null;
             }
          
          return bstClassObject;
         }

      /** Local method for getting next desired character 
       * from the file stream
       * 
       * @param rangeString set of desired characters
       *  
       * @return integer character for use in input process
       */
      private static int getNextCharInt( String rangeString )
         { 
          int nextCharInt = 0;
          
          try
             {
              nextCharInt = fileIn.read();
              
              while( nextCharInt != END_OF_FILE_MARKER 
                           && !isInString( (char)nextCharInt, rangeString ) )
                 {
                  nextCharInt = fileIn.read();
                 }
             }
          
          catch( IOException ioe )
             {
              System.out.println( "INPUT ERROR: Failure to capture character" );
             }
          
         // if( )
          
          return nextCharInt;
         }
      
      /** Local method that searches for character in a given string
       * 
       * @param testChar search character
       * 
       * @param testString string containing list of acceptable characters
       * 
       * @return true if character found in string; false otherwise
       */
      private static boolean isInString( char testChar, String testString )
      {
       int index;
         
       for( index = 0; index < testString.length(); index++ )
          {
           if( testString.charAt( index ) == testChar )
              {
               return true;
              }
          }
       
       return false;
      }
   }

