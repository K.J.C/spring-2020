package p7_package;


/**
 * Binary Search Tree (BST) class for managing generic data
 *
 * Note: Data used must have implemented Comparable interface
 *
 */
public class Generic_BST_Class <GenericData extends Comparable<GenericData>>
{
    /**
     * Root of BST
     */
    private BST_Node BST_Root;

    /**
     * Returned value reference for remove operation
     */
    private GenericData removed;

    /**
     *  Default class constructor, initializes BST
     */
    public Generic_BST_Class()
    {
        BST_Root=null;
    }

    /**
     * Clears tree
     */
    public void  clearTree()
    {
        BST_Root=null;
    }

    /**
     * Provides inOrder traversal action
     */
    public void  displayInOrder()
    {
        // Check to catch null pointers on empty trees
        if(!isEmpty())
        {
            displayInOrderHelper(BST_Root);
        }
    }

    /**
     * Provides inOrder traversal action using recursion
     * @param localRoot BST_Node tree root reference at the current
     *                 recursion level
     */
    private void  displayInOrderHelper(BST_Node localRoot)
    {
        /*

        1
      /   \
     2     3
    /  \
   4    5
        Inorder (Left, Root, Right) : 4 2 5 1 3

        */

        if(localRoot.leftChildRef != null)
        {
            displayInOrderHelper(localRoot.leftChildRef);
        }
        System.out.println(localRoot.nodeData);
        if(localRoot.rightChildRef != null)
        {
            displayInOrderHelper(localRoot.rightChildRef);
        }



    }

    /**
     *  Provides postOrder traversal action
     */
    public void displayPostOrder()
    {
        // Check to catch null pointers on empty trees
        if(!isEmpty())
        {
            displayPostOrderHelper(BST_Root);
        }
    }

    /**
     * Provides postOrder traversal action using recursion
     * @param localRoot BST_Node tree root reference at the current
     *                 recursion level
     */
    private void displayPostOrderHelper(BST_Node localRoot)
    {
 /*
        1
      /   \
     2     3
    /  \
   4    5
         Postorder (Left, Right, Root) : 4 5 2 3 1
 */
        if(localRoot.leftChildRef != null)
        {
            displayPostOrderHelper(localRoot.leftChildRef);
        }
        if(localRoot.rightChildRef != null)
        {
            displayPostOrderHelper(localRoot.rightChildRef);
        }
        System.out.println(localRoot.nodeData);

    }

    /**
     * Provides preOrder traversal action
     * <p>
     * Note: Calls displayPreOrderHelper
     */
    public void displayPreOrder()
    {
        // Check to catch null pointers on empty trees
        if(!isEmpty())
        {
            displayPreOrderHelper(BST_Root);
        }
    }

    /**
     * Provides preOrder traversal action using recursion
     * @param localRoot BST_Node tree root reference at the current
     *                 recursion level
     */
    private void displayPreOrderHelper(BST_Node localRoot)
    {
        /*
        1
      /   \
     2     3
    /  \
   4    5
         Preorder (Root, Left, Right) : 1 2 4 5 3
       */
        System.out.println(localRoot.nodeData);
        if(localRoot.leftChildRef != null)
        {
            displayPreOrderHelper(localRoot.leftChildRef);
        }
        if(localRoot.rightChildRef != null)
        {
            displayPreOrderHelper(localRoot.rightChildRef);
        }


    }

    /**
     * Insert method for BST
     * <p>
     * Note: uses insert helper method which returns root reference
     * @param inData GenericData data to be added to BST
     */
    public void insert(GenericData inData)
    {
        // Check for empty tree to prevent errors
        if(isEmpty())
        {
            BST_Root = new BST_Node(inData);
        }
        else
        {
            insertHelper(BST_Root,inData);
        }

    }

    /**
     * Insert helper method for BST insert action
     * <p>
     * Note: Recursive method returns updated local root to maintain
     * tree linkage
     * @param localRoot BST_Node tree root reference at the current
     *                  recursion level
     * @param inData GenericData item to be added to BST
     * @return BST_Node reference used to maintain tree linkage
     */
    private BST_Node insertHelper(BST_Node localRoot, GenericData inData)
    {
        //
       if (localRoot.nodeData.compareTo(inData)>0)
       {
           // if next node is open
           if(localRoot.leftChildRef == null)
           {
               localRoot.leftChildRef = new BST_Node(inData);
           }
           else
           {
               insertHelper(localRoot.leftChildRef,inData);
           }
       }
       else if(localRoot.nodeData.compareTo(inData)<0)
        {
            // if next node is open
            if(localRoot.rightChildRef == null)
            {
                localRoot.rightChildRef = new BST_Node(inData);
            }
            else
            {
                insertHelper(localRoot.rightChildRef, inData);
            }
        }
       return localRoot;
    }

    /**
     * Test for empty tree
     * @return Boolean result of test
     */
    public boolean isEmpty()
    {
        return BST_Root == null;
    }

    /**
     * Searches tree from given node to maximum value node below it,
     * stores data value found, and then unlinks the node
     * @param maxParent BST_Node reference to current node
     * @param maxLoc BST_Node reference to child node to be tested
     * @return BST_Node reference containing removed node
     */
    private BST_Node removeFromMax(BST_Node maxParent, BST_Node maxLoc)
    {
        return null;
    }

    /**
     * Removes data node from tree using given key
     * <p>
     * Note: uses remove helper method
     * <p>
     * Note: uses search initially to get value, if it is in tree;
     * if value found, remove helper method is called, otherwise returns null
     * @param inData GenericData that includes the necessary key
     * @return GenericData result of remove action
     */
    public GenericData removeItem(GenericData inData)
    {
        return null;
    }

    /**
     * Remove helper for BST remove action
     * <p>
     * Note: Recursive method returns updated local root
     * to maintain tree linkage
     * <p>
     * Note: Sets member value removed to removed data
     * for return by removeItem call
     * <p>
     * Note: uses removeFromMax method
     * @param localRoot BST_Node tree root reference
     *                  at the current recursion level
     * @param outData GenericData item that includes the necessary key
     * @return BST_Node reference result of remove helper action
     */
    private BST_Node removeItemHelper(BST_Node localRoot, GenericData outData)
    {
        return null;
    }

    /**
     * Searches for data in BST given GenericData with necessary key
     * @param searchData GenericData item containing key
     * @return GenericData reference to found data
     */
    public GenericData search(GenericData searchData)
    {
        if(!isEmpty())
        {
            return searchHelper(BST_Root,searchData);
        }
        return null;
    }

    /**
     * Helper method for BST search action
     * @param localRoot BST_Node tree root reference
     *                  at the current recursion level
     * @param searchData GenericData item containing key
     * @return GenericData item found
     */
    private GenericData searchHelper(BST_Node localRoot, GenericData searchData)
    {
        GenericData resultOfSearch;
        if(searchData.compareTo(localRoot.nodeData)==0)
        {
            return localRoot.nodeData;
        }

        if(localRoot.leftChildRef != null)
        {
            // recurses down left side and stores the result
            resultOfSearch = searchHelper(localRoot.leftChildRef,searchData);
            if(resultOfSearch != null)
            {
                return resultOfSearch;
            }
        }

        if(localRoot.rightChildRef != null)
        {
            // recurses down left side and stores the result
            resultOfSearch = searchHelper(localRoot.rightChildRef,searchData);
            if(resultOfSearch != null)
            {
                return resultOfSearch;
            }
        }
        return null;
    }

    /**
     * Binary Search Tree node class for managing generic data
     */
    private class BST_Node
    {
        /**
         * Member value left child reference
         */
        private BST_Node leftChildRef;

        /**
         * Member value GenericData node
         */
        private GenericData nodeData;

        /**
         * Member value right child reference
         */
        private BST_Node rightChildRef;

        /**
         * Copy constructor for data
         * <p>
         * Note: Not used in class but available to user
         * @param copied GenericData quantity
         */
        public BST_Node(BST_Node copied)
        {
            leftChildRef=copied.leftChildRef;
            rightChildRef= copied.rightChildRef;
            nodeData= copied.nodeData;
        }

        /**
         * Initialization constructor for data
         * @param inData GenericData quantity
         */
        public BST_Node(GenericData inData)
        {
            leftChildRef=null;
            rightChildRef=null;
            nodeData=inData;
        }

    }


}
