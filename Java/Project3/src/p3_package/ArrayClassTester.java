package p3_package;

public class ArrayClassTester
{
    public static void main(String[] args)
    {

        int index;

        ArrayClass defaultTester = new ArrayClass();
        for(index=0;index<defaultTester.getCurrentCapacity();index++)
        {
            defaultTester.insertItemAt(index,getRandBetween(0,99));
        }
        ArrayClass copyTester = new ArrayClass(defaultTester);
        System.out.println("Normal Print");
        print(defaultTester);
        System.out.println("Removing items at index 16 0 3 5");
        int var1, var2, var3,var4;
        var1=defaultTester.removeItemAt(16);
        var2=defaultTester.removeItemAt(0);
        var3=defaultTester.removeItemAt(3);
        var4=defaultTester.removeItemAt(5);
        System.out.println(var1);
        System.out.println(var2);
        System.out.println(var3);
        System.out.println(var4);
        System.out.println("Printing After Removal");
        print(defaultTester);
        System.out.println("Readding Items");
        System.out.println(defaultTester.insertItemAt(16,var1));
        System.out.println(defaultTester.insertItemAt(0,var2));
        System.out.println(defaultTester.insertItemAt(3,var3));
        System.out.println(defaultTester.insertItemAt(5,var4));
        print(defaultTester);
        System.out.println("Running Shell Sort ");
        defaultTester.runShellSort();
        System.out.println("Printing Shell Sorted Array");
        print(defaultTester);
        System.out.println("Loading Unique Randoms");
        System.out.println("Loaded Correctly: "+ defaultTester.loadUniqueRandoms(20,0,99));
        System.out.println("Printing Randoms");
        print(defaultTester);
        System.out.println("Running MergeSort");
        copyTester.runMergeSort();
        System.out.println("Printing MergeSort Array");
        print(copyTester);
        System.out.println("Running QuickSort");
        defaultTester.runQuickSort();
        System.out.println("Printing QuickSort Array");
        print(defaultTester);
/*
        // cap block
        ArrayClass capacityTester = new ArrayClass(5);

        for(index=0;index<capacityTester.getCurrentCapacity();index++)
        {
            capacityTester.insertItemAt(index,index);
        }
        print(capacityTester);
        capacityTester.resize(7);
        for(index=0;index<capacityTester.getCurrentCapacity();index++)
        {
            capacityTester.insertItemAt(index,index);
        }
        print(capacityTester);
        capacityTester.clear();
        print(capacityTester);
*/

/*
        // tester/copier block
        ArrayClass tester = new ArrayClass(3, 1,69);
        print(tester);
        tester.appendItem(4);
        print(tester);
        tester.insertItemAt(1,5);
        System.out.println("Old Array");
        print(tester);

        ArrayClass copyTester = new ArrayClass(tester);
        System.out.println("copied Array");
        copyTester.resize(5);
        print(copyTester);
        for(index=0;index<copyTester.getCurrentCapacity();index++)
        {
            copyTester.insertItemAt(index,index);
        }

        print(copyTester);
*/
    }
    public static void print(ArrayClass printMe)
    {
        int index;
        for(index=0;index<printMe.getCurrentSize();index++)
        {
                System.out.print(printMe.accessItemAt(index)+" ");
        }
        System.out.println("");
    }

    public static void fullPrint(ArrayClass printMe)
    {
        int index;
        for(index=0;index<printMe.getCurrentCapacity();index++)
        {
                System.out.print(printMe.accessItemAt(index)+" ");
        }
        System.out.println("");
    }
    public static int getRandBetween( int low, int high )
    {
        int value, range = high - low + 2;
        value = (int)( Math.random() * range );
        return low + value;
    }

    public static class ArrayClassTesterbak
    {
        public static void main(String[] args)
        {

            int index;


            ArrayClass defaultTester = new ArrayClass();
            for(index=0;index<defaultTester.getCurrentCapacity();index++)
            {
                defaultTester.insertItemAt(index,getRandBetween(0,99));
            }
            System.out.println("Normal Print");
            print(defaultTester);
            System.out.println("Removing items at 16 0 3 5");
            System.out.println(defaultTester.removeItemAt(16));
            System.out.println(defaultTester.removeItemAt(0));
            System.out.println(defaultTester.removeItemAt(3));
            System.out.println(defaultTester.removeItemAt(5));
            System.out.println("Printing After Removal");
            print(defaultTester);
            defaultTester.runShellSort();



    /*
            // cap block
            ArrayClass capacityTester = new ArrayClass(5);

            for(index=0;index<capacityTester.getCurrentCapacity();index++)
            {
                capacityTester.insertItemAt(index,index);
            }
            print(capacityTester);
            capacityTester.resize(7);
            for(index=0;index<capacityTester.getCurrentCapacity();index++)
            {
                capacityTester.insertItemAt(index,index);
            }
            print(capacityTester);
            capacityTester.clear();
            print(capacityTester);
    */

    /*
            // tester/copier block
            ArrayClass tester = new ArrayClass(3, 1,69);
            print(tester);
            tester.appendItem(4);
            print(tester);
            tester.insertItemAt(1,5);
            System.out.println("Old Array");
            print(tester);

            ArrayClass copyTester = new ArrayClass(tester);
            System.out.println("copied Array");
            copyTester.resize(5);
            print(copyTester);
            for(index=0;index<copyTester.getCurrentCapacity();index++)
            {
                copyTester.insertItemAt(index,index);
            }

            print(copyTester);
    */
        }
        public static void print(ArrayClass printMe)
        {
            int index;
            for(index=0;index<printMe.getCurrentSize();index++)
            {
                System.out.print(printMe.accessItemAt(index)+" ");
            }
            System.out.println("");
        }

        public static void fullPrint(ArrayClass printMe)
        {
            int index;
            for(index=0;index<printMe.getCurrentCapacity();index++)
            {
                System.out.print(printMe.accessItemAt(index)+" ");
            }
            System.out.println("");
        }
        public static int getRandBetween( int low, int high )
        {
            int value, range = high - low + 2;
            value = (int)( Math.random() * range );
            return low + value;
        }
    }
}
