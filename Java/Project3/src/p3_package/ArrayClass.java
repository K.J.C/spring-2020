package p3_package;

/**
 * Description: Class wrapper for a Java array,
 * with additional management operations
 * <p>
 * Note: Maintains a capacity value for maximum number of
 * items that can be stored,
 * and a size value for the number of valid or viable data items in the array
 *
 * @author  Kyler Carling
 * @version 1.1
 * @since   Feb 5 2020
 */
public class ArrayClass
{
    /**
     * Maximum Possible Size
     */
    private int arrayCapacity;
    /**
     * Current number of valid values
     */
    private int arraySize;
    /**
     * Default value of the capacity variable
     */
    private static final int DEFAULT_CAPACITY=10;
    /**
     * Exit code indicated a failure to access the requested resource
     */
    public static final int FAILED_ACCESS=-999999;
    /**
     * The actual array that this program wraps around
     */
    private int[] localArray;

    /**
     * Default constructor, initializes array to default capacity
     */

    public ArrayClass()
    {
        arrayCapacity=DEFAULT_CAPACITY;
        arraySize=0;
        localArray= new int[DEFAULT_CAPACITY];
    }

    /**
     * Copy constructor,
     * initializes array to size and capacity of copied array,
     * then copies only the elements up to the given size
     * @param copied  ArrayClass object to be copied
     */

    public ArrayClass(ArrayClass copied)
    {
        this.arraySize=copied.getCurrentSize();
        this.arrayCapacity=copied.getCurrentCapacity();
        localArray = new int[getCurrentCapacity()];
        int arrayIterator=0;
        while(arrayIterator<arraySize)
        {
            this.localArray[arrayIterator]=copied.accessItemAt(arrayIterator);
            arrayIterator++;
        }
    }

    /**
     * Initializing constructor, initializes array to specified capacity
     * @param capacity integer maximum capacity specification for the array
     */

    public ArrayClass(int capacity)
    {
        arrayCapacity=capacity;
        arraySize=0;
        localArray= new int[arrayCapacity];
    }

    /**
     * Initializing constructor, initializes array to specified capacity,
     * size to specified value,
     * then fills all elements with specified size value
     * @param capacity maximum capacity specification for the array
     * @param size sets the number of items to be filled in array,
     *            and sets the size of the ArrayClass object <p>
     * @param fillValue value to be placed in all elements <p>
     *                  of initialized array up to the size
     */

    public ArrayClass(int capacity, int size, int fillValue)
    {
        arrayCapacity=capacity;
        localArray= new int[arrayCapacity];
        arraySize=size;
        int localArrayIterator=0;
        while(localArrayIterator<=arraySize)
        {
            localArray[localArrayIterator]=fillValue;
            localArrayIterator++;
        }
    }

    /**
     * Accesses item in array at specified index
     * if index within array size bounds
     * @param accessIndex index of requested element value
     * @return accessed value if successful, FAILED_ACCESS (-999999) if not
     */

    public int accessItemAt(int accessIndex)
    {
        if(accessIndex<getCurrentSize())
        {
            return this.localArray[accessIndex];
        }
        return FAILED_ACCESS;
    }

    /**
     * Appends item to end of array, if array is not full,
     *  e.g., no more values can be added
     * @param newValue value to be appended to array
     * @return Boolean success if appended, or fai        System.out.println(defaultTester.accessItemAt(10));lure if array was full
     */

    public boolean appendItem(int newValue)
    {
        if(isFull())
        {
            return false;
        }
        // Adds newValue to position at size and increments size
        arraySize++;
        int indexToAppendValue=getCurrentSize()-1;
        localArray[indexToAppendValue]=newValue;
        return true;
    }

    /**
     * Clears array of all valid values by setting array size to zero,
     * values remain in array but are not accessible
     */

    public void clear()
    {
        arraySize=0;
    }

    /**
     * Simple array dump for testing purposes
     */
    private void dump()
    {
        int arrayIndex=0;
        while(arrayIndex<arraySize)
        {
            System.out.println(localArray[arrayIndex]);
            arrayIndex++;
        }
    }

    /**
     * Description: Gets current capacity of array
     *<p>
     * Note: capacity of array indicates number of values the array can hold
     * @return capacity of array
     */

    public int getCurrentCapacity()
    {
        return arrayCapacity;
    }

    /**
     * Description: Gets current size of array
     *<p>
     * Note: size of array indicates number
     * of valid or viable values in the array
     * @return size of array
     */

    public int getCurrentSize()
    {
        return arraySize;
    }

    /**
     * Generates random number between given low and high values
     *
     * @param low lowest value that will be generated by method
     *
     * @param high highest value that will be generated by method
     *
     * @return the generated random value
     */
    public int getRandBetween( int low, int high )
    {
        int value, range = high - low + 1;
        value = (int)( Math.random() * range );
        return low + value;
    }

    /**
     * Description: Inserts item to array at specified index
     * if array is not full, e.g., no more values can be added
     *<p>
     * Note: Value is inserted at given index,
     * all data from that index to the end of the array is shifted up by one
     *<p>
     * Note: Value can be inserted after the last valid element
     * but not at any index past that point
     * @param insertIndex index of element into which value is to be inserted
     * @param newValue value to be inserted into array
     * @return Boolean success if inserted, or failure if array was full
     */

    public boolean insertItemAt(int insertIndex, int newValue)
    {
        //Gatekeeper block to throw out bad method calls
        if(isFull() || insertIndex>getCurrentSize()+1 || insertIndex<0)
        {
            return false;
        }

        int positionIndex=getCurrentSize();
        arraySize++;
        // 1 offset to prevent numbers from being pushed out of array.
        if(positionIndex==getCurrentCapacity()-1)
        {
            positionIndex--;
        }

        while(positionIndex>=insertIndex)
        {
            localArray[positionIndex+1]=localArray[positionIndex];
            positionIndex--;
        }
        localArray[insertIndex]=newValue;
        return true;
    }

    /**
     * Tests for size of array equal to zero, no valid values stored in array
     * @return Boolean result of test for empty
     */

    public boolean isEmpty()
    {
        return getCurrentSize()==0;
    }

    /**
     * Tests for size of array equal to capacity, no more values can be added
     * @return Boolean result of test for full
     */

    public boolean isFull()
    {
        return getCurrentSize()==getCurrentCapacity();
    }

    /**
     * Tests for value found in object array;
     * returns true if value within array, false otherwise
     * @param testVal value to be tested
     * @return boolean true if value is found in array, false otherwise
     */
    public boolean isInArray(int testVal)
    {
        int index=0;
        while(index<arraySize-1)
        {
            if(localArray[index]==testVal)
            {
                return true;
            }
            index++;
        }
        return false;
    }

    /**
     * Removes item from array at specified index
     * if index within array size bounds
     * <p>
     * Note: Each data item from the element immediately
     * above the remove index to the end of the array
     * is moved down by one element
     * @param removeIndex index of element value to be removed
     * @return the value contained at the removeIndex, otherwise FAILED_ACCESS
     */

    public int removeItemAt(int removeIndex)
    {
        //Gatekeeper block to throw out bad method calls
        if(isEmpty() || removeIndex<0 || removeIndex>getCurrentSize()-1)
        {
            return FAILED_ACCESS;
        }

        int indexToDelete=accessItemAt(removeIndex);

        int posIndex=removeIndex;

        while(posIndex<getCurrentCapacity()-1)
        {
            localArray[posIndex]=localArray[posIndex+1];
            posIndex++;
        }
        arraySize--;
        return indexToDelete;
    }

    /**
     * Loads a specified number of unique random numbers in object
     * <p>
     * Note: This method overwrites all data in the array up to
     * the number of randoms requested
     * <p>
     * Note: If requested number of randoms is greater than the array capacity,
     * the array is resized
     * <p>
     * Note: Size is set to number of random numbers requested
     * <p>
     * Exceptional Condition: If more values are requested
     * than are possible given the range of numbers,
     * method returns false, otherwise, it returns true
     *
     * @param numRands number of random values requested
     * @param lowLimit lowest value to be generated
     * @param highLimit highest value to be generated
     * @return boolean true if method sucessful; false otherwise
     */
    public boolean loadUniqueRandoms(int numRands, int lowLimit, int highLimit)
    {
        // gatekeeper block here
        // rejects the method call if
        // invalid args
        if(highLimit-lowLimit<numRands)
        {
            return false;
        }

        if(numRands>arrayCapacity)
        {
            resize(numRands);
        }
        int currentRandNumber=getRandBetween(lowLimit,highLimit);
        int indexToPopulate=0;
        arraySize=numRands;

        while(indexToPopulate<numRands)
        {
            if(isInArray(currentRandNumber))
            {
                currentRandNumber=getRandBetween(lowLimit,highLimit);
            }
            else
            {
                localArray[indexToPopulate]= currentRandNumber;
                indexToPopulate++;
            }
        }
        return true;
    }

    /**
     * Description: Resets array capacity, copies current size
     * and current size number of elements
     *<p>
     * Exception: Method will not resize capacity
     * below current array capacity,
     * returns false if this is attempted, true otherwise
     * new array copies the size and contents of the old array
     * @param newCapacity new capacity to be set;
     * must be larger than current capacity
     * @return Boolean condition of resize success or failure
     */

    public boolean resize(int newCapacity)
    {
        if(newCapacity<=getCurrentCapacity())
        {
            return false;
        }

        int[] resizedArr = new int[newCapacity];
        int arrayIterator=0;
        while(arrayIterator<getCurrentSize())
        {
            resizedArr[arrayIterator]=accessItemAt(arrayIterator);
            arrayIterator++;
        }
        arrayCapacity=newCapacity;

        this.localArray=resizedArr;
        return true;
    }

    /**
     * Merges values brought in between a low and high index segment of an array
     * Note: uses locally sized single array for temporary storage
     *
     * @param lowIndex lowest index of array segment to be managed
     * @param middleIndex middle index of array segment to be managed
     * @param highIndex high index of array segment to be managed
     */
    private void runMerge(int lowIndex,int middleIndex,int highIndex)
    {
        int leftSideIndex= lowIndex;
        int rightSideIndex = middleIndex + 1;
        int currentPosition = leftSideIndex;
        int[] tempStorageArray = new int[arrayCapacity];

        // Transfers from localArray to
        // the local storage array 1 index at a time
        // Kinda confusing
        int transferIndex=lowIndex;
        while(transferIndex<=highIndex)
        {
            tempStorageArray[transferIndex]=
                    localArray[transferIndex];
            transferIndex++;
        }
        //While either are true
        while(leftSideIndex<=middleIndex || rightSideIndex<=highIndex)
        {
            // If both sides are not finished processing
            if(leftSideIndex <= middleIndex && rightSideIndex <= highIndex)
            {
                // if left value is higher place in solved
                if(tempStorageArray[leftSideIndex] <=
                        tempStorageArray[rightSideIndex])
                {
                    localArray[currentPosition] =
                            tempStorageArray[leftSideIndex];
                    leftSideIndex++;
                }
                // else place right value in solved
                else
                {
                    localArray[currentPosition] =
                            tempStorageArray[rightSideIndex];
                    rightSideIndex++;
                }
            }
            // If only one side isnt finished processing
            // Sort Left Side
            else if(leftSideIndex <= middleIndex)
            {
                localArray[currentPosition] =
                        tempStorageArray[leftSideIndex];
                leftSideIndex++;
            }
            // Sort Right Side
            else if(rightSideIndex <= highIndex)
            {
                localArray[currentPosition] =
                        tempStorageArray[rightSideIndex];
                rightSideIndex++;
            }
            currentPosition++;
        }

    }

    /**
     * Data sorted using merge sort algorithm
     * Note: Call runMergeSortHelper with lower and
     * upper indices of array to be sorted
     */
    public void runMergeSort()
    {
        //Gatekeeper block here
        //to check if the list has been
        //already sorted
        if(arraySize>1)
        {
            //otherwise run the helper
            runMergeSortHelper(0,arraySize-1);
        }


    }

    /**
     * Merge sort helper, places low and
     * high indices of array segment to
     * be processed into recursive method,
     * then sorts data using merge sort algorithm
     *
     * @param lowIndex lowest index of array segment to be managed;
     *                 this varies as the segments are broken down recursively
     *
     * @param highIndex highest index of array segment to be managed;
     *                  this varies as the segments are broken down recursively
     */
    private void runMergeSortHelper(int lowIndex, int highIndex)
    {
        int middle;
        //Keep recursive splitting until highIndex and lowIndex are equal
        if(highIndex-lowIndex>0)
        {
            middle = ( lowIndex + highIndex ) / 2;
            runMergeSortHelper( lowIndex, middle );
            runMergeSortHelper( middle + 1, highIndex );
            runMerge( lowIndex, middle, highIndex );
        }

    }

    /**
     * partitions array using the first value as the partition;
     * when this method is complete the partition value is in
     * the correct location in the array
     * @param lowIndex low index of array segment to be partitioned
     * @param highIndex high index of array segment to be partitioned
     * @return integer index of partition pivot
     */
    private int runPartition(int lowIndex,int highIndex)
    {
        // First index chosen as pivot point
        int pivotPoint = localArray[lowIndex];

        while(lowIndex <= highIndex)
        {
            // Move right until higher number is found
            while(localArray[lowIndex] < pivotPoint)
            {
                lowIndex++;
            }
            // move left until lower number is found
            while(localArray[highIndex] > pivotPoint)
            {
                highIndex--;
            }
            // If left number is higher or equal to right num swap them
            if(lowIndex <= highIndex)
            {
                swapValuesAtIndex(lowIndex,highIndex);
                lowIndex++;
                highIndex--;
            }
        }
        return lowIndex;
    }

    /**
     * Data sorted using quick sort algorithm
     * Note: Call runQuickSortHelper with lower and upper
     * indices of array to be sorted
     */
    public void runQuickSort()
    {
        //Gatekeeper block here
        //to check if the list has been
        //already sorted
        if(arraySize>1)
        {
            //otherwise run the helper
            runQuickSortHelper(0,arraySize-1);
        }

    }

    /**
     * helper method run with parameters that support recursive access
     * @param lowIndex low index of the segment of the array to be processed
     * @param highIndex high index of the segment of the array to be processed
     */
    private void runQuickSortHelper(int lowIndex, int highIndex)
    {

        int index = runPartition( lowIndex, highIndex );

        if(lowIndex < index - 1)
        {
            runQuickSortHelper(lowIndex, index-1);
        }

        if(index < highIndex)
        {
            runQuickSortHelper(index, highIndex);
        }
    }

    /**
     * Sorts data using the Shell's sort algorithm
     */
    public void runShellSort()
    {
        int gaplength;
        int innerGapIndex;
        int insertionIndex;
        int tempItem;
        int itemToTest;
        boolean theSearchContinues;

        for( gaplength = arraySize/2; gaplength > 0; gaplength /= 2 )
        {
            for( innerGapIndex = gaplength;
                 innerGapIndex < arraySize; innerGapIndex++ )
            {
                tempItem = localArray[innerGapIndex];
                insertionIndex = innerGapIndex;
                theSearchContinues = true;
                while( theSearchContinues && insertionIndex >= gaplength )
                {
                    itemToTest
                            = localArray[ insertionIndex - gaplength ];

                    if( itemToTest >tempItem )
                    {
                        localArray[ insertionIndex ]
                                = localArray[ insertionIndex - gaplength ];
                        insertionIndex -= gaplength;
                    }
                    else
                    {
                        theSearchContinues = false;
                    }
                }
                localArray[ insertionIndex ] = tempItem;
            }
        }
    }

    /**
     * swaps values in the object array by taking in the indices
     * of the array locations
     * <p>
     * Note: for a small level of optimization, this method does
     * not swap values if the indices are the same
     *
     * @param oneIndex index of the of the values to be swapped
     *
     * @param otherIndex index of the other value to be swapped
     */
    private void swapValuesAtIndex( int oneIndex, int otherIndex )
    {
        int temp = localArray[ oneIndex ];

        if( oneIndex != otherIndex )
        {
            localArray[ oneIndex ] = localArray[ otherIndex ];

            localArray[ otherIndex ] = temp;
        }
    }


}
